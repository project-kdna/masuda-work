#ifndef __INCLUDED_DEBUGUTILITY_H__
#define __INCLUDED_DEBUGUTILITY_H__

#include <tchar.h>
#include <Windows.h>

#ifdef _DEBUG
#define OutputDebugStringF(str, ...) \
		{ \
		TCHAR c[256]; \
		_stprintf_s(c, str, __VA_ARGS__); \
		OutputDebugString(c); \
		}

#else 
#define OutputDebugStringF(str, ...)
#endif

#endif // !__INCLUDED_DEBUGUTILITY_H__
