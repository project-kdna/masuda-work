#ifndef __INCLUDED_COMMON_H__
#define __INCLUDED_COMMON_H__

#pragma comment(lib, "DxLib.lib")

#include <Windows.h>
#include <tchar.h>
#include <iostream>
#include <io.h>
#include <fcntl.h>
#include <cassert>

// �񋓌^
enum EVT : int
{
	NONE = 0,
	START,
	ENDING,
	BATTLEMODE,
	ADVENTUREMODE,
	CANCEL,
	QUIT,
	LENGTH
};

// �萔
struct CPARAM {
	static const int WINDOW_WIDTH;
	static const int WINDOW_HEIGHT;
	static const int POP_CHIP_SIZE_X;
	static const int POP_CHIP_SIZE_Y;
	static const int POP_OFFSET_X;
	static const int POP_OFFSET_Y;
	static const int POP_TEXT_BUFFER_SIZE;
};

void createConsoleWindow();
void closeConsoleWindow();

#endif // !__INCLUDED_COMMON_H__
