#ifndef __INCLUDED_COLLECTOR_H__
#define __INCLUDED_COLLECTOR_H__

#include <string>
#include <unordered_map>

class Collector
{
public:
	void setValue(const std::string& key, void* value) {
		m_hash[key] = value;
	}

	template <typename T> T* getValue(const std::string& key) {
		return (T*)m_hash[key];
	}

private:
	std::unordered_map<std::string, void*> m_hash;
};

#endif // !__INCLUDED_COLLECTOR_H__
