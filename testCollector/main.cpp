#include <iostream>
#include <Windows.h>
#include <tchar.h>
#include "DebugUtility.h"
#include "common.h"
#include "Collector.h"

int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int) {
	createConsoleWindow();

	Collector coll;
	int a = 123456789;
	char b = 'b';
	std::string c = "こんにちわ";
	coll.setValue("a", &a);
	coll.setValue("b", &b);
	coll.setValue("c", &c);
	
	int* aa = coll.getValue<int>("a");
	char* bb = coll.getValue<char>("b");
	std::string* cc = coll.getValue<std::string>("c");

	std::cout << "a: " << a << "\nb: " << b << "\nc: " << c << "\n\n";
	std::cout << "aa: " << aa << "\nbb: " << &bb << "\ncc: " << &cc << "\n\n";

	system("pause");
	closeConsoleWindow();
	return 0;
}