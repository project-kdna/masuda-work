#include "common.h"

// �萔
const int CPARAM::WINDOW_WIDTH = 640;
const int CPARAM::WINDOW_HEIGHT = 480;
const int CPARAM::POP_CHIP_SIZE_X = 16;
const int CPARAM::POP_CHIP_SIZE_Y = 16;
const int CPARAM::POP_OFFSET_X = 16;
const int CPARAM::POP_OFFSET_Y = 16;
const int CPARAM::POP_TEXT_BUFFER_SIZE = 327680;

static int hDebugConsoleWindow = 0;

void createConsoleWindow()
{
	AllocConsole();
	hDebugConsoleWindow = _open_osfhandle((long)GetStdHandle(STD_OUTPUT_HANDLE), _O_TEXT);
	*stdout = *_fdopen(hDebugConsoleWindow, "w");
	setvbuf(stdout, NULL, _IONBF, 0);
}

void closeConsoleWindow()
{
	_close(hDebugConsoleWindow);
}