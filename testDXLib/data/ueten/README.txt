﻿Ueten（うえてん）フォント
=========================

Copyright (C) 2002-2014 M+ FONTS PROJECT



Description
-----------

濁点を、文字の上に付けた
TrueTypeアウトラインフォントです。


Feature
-------

- 濁点・半濁点を、文字の上に付けました。

- 「あ」＋「゛」のような字形を収録。

- 丸ゴシック。

- フリーフォントです。

詳しくは以下を参照。
http://itouhiro.hatenablog.com/entry/20140502/font



License
-------

M+ FONT LICENSE

M+ FONT LICENSEについては、配布物に含まれる
mplus-TESTFLIGHT-058/LICENSE_E
をご覧ください。


--
Itou Hiroki <itouhiro at users dot sourceforge dot jp>
