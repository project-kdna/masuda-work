Rounded M+ FONTS
Rounded M+ 1.058.20140226 (based on M+ FONTS TESTFLIGHT-058)


'Rounded M+ FONTS' is a modification of 'M+ OUTLINE FONTS'
in rounded edges.

The license of this font is the same as 'M+ OUTLINE FONTS'.

This release is built with the source of
'M+ OUTLINE FONTS' on 2014-02-26T19:00:00+09:00.


The detailed description (Japanese language)
http://sites.google.com/site/roundedmplus/

Rounded M+ FONTS distribution page (Japanese language)
http://sites.google.com/site/roundedmplus/download

Original description by itouhiro (Japanese language)
http://d.hatena.ne.jp/itouhiro/20120226
