#ifndef __INCLUDED_SCENE_H__
#define __INCLUDED_SCENE_H__

#include "common.h"
#include <stdio.h>
#include <iostream>
#include <vector>

namespace kdna {

class Scene
{
public:
	Scene(Scene* parent);
	virtual ~Scene();

	virtual void initialize() = 0;
	virtual Scene* run() = 0;
	virtual void draw() = 0;

	Scene* receiveMessage(Scene* caller, int message);
	virtual Scene* processMessage(Scene* caller, int message);
	void validate();
	void release();
	void destroy();

	Scene* getParent() { return mp_parent; }

protected:
	Scene* mp_parent;						//!< 自分の親シーン
	std::vector<Scene*> mp_childs;			//!< 子供リスト

	bool m_isValid;							//!< オブジェクトが生きてるか死んでるか
};

}
#endif // !__INCLUDED_SCENE_H__
