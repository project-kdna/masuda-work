#include "Interpolator.h"

namespace kdna {

double Interpolator::lerp(double a, double b, double s) {
	return a * (1.0 - s) + b * s;
}

}