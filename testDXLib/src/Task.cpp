/*! @file Task.cpp 
	@brief タスククラス
	@author Shohei Masuda
*/
#include "Task.h"

/*! @brief コンストラクタ */
Task::Task()
{
	m_name = "";
	m_isAutoDelete = true;
	m_priority = 0;
	m_isValid = false;
}

/*! @brief デストラクタ */
Task::~Task() {}
