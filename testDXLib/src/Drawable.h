#ifndef __INCLUDED_DRAWABLE_H__
#define __INCLUDED_DRAWABLE_H__

#include "Utility.h"

namespace kdna {

class Drawable
{
public:
	Drawable() :
		m_x(0), m_y(0), m_width(0), m_height(0),
		m_r(0), m_g(0), m_b(0), m_alpha(1.0),
		m_isVisible(true) {}
	virtual ~Drawable() {}

	virtual void draw() = 0;

	void setPosition(int x, int y);
	void setHexColor(int hexColor);
	void setColor(int r, int g, int b);
	void setAlpha(double alpha);
	void setIsVisible(bool isVisible);
	int getX() const;
	int getY() const;
	int getWidth() const;
	int getHeight() const;
	int getR() const;
	int getG() const;
	int getB() const;
	int getHexColor() const;
	double getAlpha() const;
	bool getIsVisible() const;

protected:
	void setSize(int width, int height);

private:
	int m_x;
	int m_y;
	int m_width;
	int m_height;
	int m_r;
	int m_g;
	int m_b;
	double m_alpha;
	bool m_isVisible;
};


inline void Drawable::setPosition(int x, int y) {
	m_x = x; m_y = y;
}
inline void Drawable::setSize(int width, int height) {
	m_width = width; m_height = height;
}
inline void Drawable::setHexColor(int hexColor) {
	util::cvtColorToRGB(hexColor, m_r, m_g, m_b);
}
inline void Drawable::setColor(int r, int g, int b) {
	m_r = r; m_g = g; m_b = b;
}
inline void Drawable::setAlpha(double alpha) {
	if (alpha < 0.0) { alpha = 0.0; }
	if (alpha > 1.0) { alpha = 1.0; }
	m_alpha = alpha;
}
inline void Drawable::setIsVisible(bool isVisible) {
	m_isVisible = isVisible;
}
inline int Drawable::getX() const { return m_x; }
inline int Drawable::getY() const { return m_y; }
inline int Drawable::getWidth() const { return m_width; }
inline int Drawable::getHeight() const { return m_height; }
inline int Drawable::getR() const { return m_r; }
inline int Drawable::getG() const { return m_g; }
inline int Drawable::getB() const { return m_b; }
inline int Drawable::getHexColor() const {
	int hexColor; util::cvtColorToHex(m_r, m_g, m_b, hexColor); return hexColor;
}
inline double Drawable::getAlpha() const { return m_alpha; }
inline bool Drawable::getIsVisible() const { return m_isVisible; }

}
#endif // !__INCLUDED_DRAWABLE_H__

