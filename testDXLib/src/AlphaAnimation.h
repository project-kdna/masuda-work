#ifndef __INCLUDED_ALPHAANIMATION_H__
#define __INCLUDED_ALPHAANIMATION_H__

#include "Animation.h"
#include "LinearInterpolator.h"

namespace kdna {

class AlphaAnimation
	: public Animation
{
public:
	AlphaAnimation();
	AlphaAnimation(Drawable* drawable, double start, double end, int msec);

	void reset();	// override
	void start();	// override

	void setRegion(double start, double end);

private:
	double m_initPoint;
	double m_targetPoint;
	LinearInterpolator m_li;
};

inline void AlphaAnimation::setRegion(double start, double end)
{
	if (!getHasEnded()) { return; }
	m_initPoint = start;
	m_targetPoint = end;
}

}
#endif // !__INCLUDED_ALPHAANIMATION_H__
