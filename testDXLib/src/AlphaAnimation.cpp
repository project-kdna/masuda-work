#include "AlphaAnimation.h"
#include "Drawable.h"

namespace kdna {

AlphaAnimation::AlphaAnimation() {
	setRegion(0.0, 0.0);
	setInterpolator(&m_li);
}

AlphaAnimation::AlphaAnimation(Drawable* drawable, double start, double end, int msec) {
	setDrawable(drawable);
	setRegion(start, end);
	setDuration(msec);
	setInterpolator(&m_li);
}

void AlphaAnimation::start() {
	setHasEnded(false);

	double normalizedTime = getNormalizedTime();
	double v = getInterpolator().getInterpolator(normalizedTime);

	double alpha = Interpolator::lerp(m_initPoint, m_targetPoint, v);
	getDrawable().setAlpha(alpha);

	if (alpha == m_targetPoint) {
		setHasEnded(true);
	}
}

void AlphaAnimation::reset() {

}

}