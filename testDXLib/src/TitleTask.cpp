#include "TitleTask.h"

/*! @brief コンストラクタ */
TitleTask::TitleTask()
{
}

/*! @brief デストラクタ */
TitleTask::~TitleTask()
{
}

/*! @brief 初期化*/
void TitleTask::initialize()
{
	/*
	// 画像の読み込み
	m_gp.setInitAlpha(255.0);
	m_gp.create("bg", "data/title.png");
	m_gp.create("arrow", "data/arrow.png");
	// 画像の設定
	m_gp["bg"]->setAlpha(0.0);
	m_gp["arrow"]->setPosition(50, 50);

	// 文章の読み込み
	m_tp.setInitColor(0, 0, 0);				// 初期色の設定
	m_tp.setInitAlpha(255.0);
	m_tp.create("start", "GameStart");
	m_tp.create("load", "LoadGame");
	m_tp.create("quit", "Quit");
	m_tp.create("isquit", "ゲームを終了します．\nよろしいですか？");

	// 文章の設定
	m_tp["start"]->setPosition(100, 50);
	m_tp["load"]->setPosition(100, 100);
	m_tp["quit"]->setPosition(100, 150);
	m_tp["isquit"]->setPosition(100, 50);
	m_tp["isquit"]->setAlpha(0.0);
	m_tp["isquit"]->setColor(255, 255, 255);

	// アニメーションの追加
	m_amgr.append(new FadeAnime(m_gp["bg"], 500, 255.0));

	// その他初期化
	m_idx = 0;

	m_mbox.loadImage("data/Window_a_00_.png", 16, 16);
	m_mbox.setRect(100, 100, 250, 150);
	m_mbox.setAlpha(0.0);

	// 状態の初期化
	m_taskState = State::Start;
	*/
}

/** @brief 実行 */
int TitleTask::run()
{
	// 管理の取得
	DrawManager* dmgr = DrawManager::getInstance();
	InputManager* imgr = InputManager::getInstance();

	if (m_taskState == State::Start)
	{
		// アニメーションの実行
		if (m_amgr.run()) { m_taskState = State::Select; }
	}
	if (m_taskState == State::Select)
	{
		// 選択する
		this->selectItem();
		// 描画
		dmgr->append(m_tp["start"], 11);
		dmgr->append(m_tp["load"], 11);
		dmgr->append(m_tp["quit"], 11);
		
		dmgr->append(m_gp["arrow"], 11);
		
	}
	if (m_taskState == State::Selected)
	{
		// 決定時のインデックス番号によって処理を変える
		switch (static_cast<Item>(m_idx))
		{
			// 新しく始める
			case Item::NewGame:
				//return EVT::OPENING;
				m_mbox.setPosition(100, 100);
				m_mbox.setAlpha(255.0);
				m_tp["isquit"]->setPosition(116, 116);
				m_tp["isquit"]->setAlpha(255.0);
				break;

				// ロードする
			case Item::LoadGame:
				// ロードタスクに状態遷移する
				mp_next = new SaveLoadTask();
				// 閉じるアニメーションの追加
				m_amgr.release();
				m_amgr.append(new FadeAnime(0, 500, 0.0));
				// 状態遷移
				m_taskState = State::End;
				break;

				// 終了する
			case Item::Quit:
				return EVT::QUIT;
				break;
		}
		dmgr->append(m_tp["isquit"], 13);
		dmgr->append(&m_mbox, 12);
	}
	if (m_taskState == State::End)
	{
		// アニメーションの実行
		if (m_amgr.run()) {
			m_taskState = State::Select;
			TaskManager::getInstance()->append(mp_next);
			TaskManager::getInstance()->remove(this);
		}
	}

	// 描画
	dmgr->append(m_gp["bg"], 0);

	return EVT::NONE;
}


/*! @brief 項目の選択 */
void TitleTask::selectItem()
{
	InputManager* imgr = InputManager::getInstance();
	int x, y;
	int prev = m_idx;
	m_gp["arrow"]->getPosition(x, y);

	// カーソル移動
	if (imgr->getConfigKeyState(VK::UP, false)) {
		m_idx--;
	}
	if (imgr->getConfigKeyState(VK::DOWN, false)) {
		m_idx++;
	}
	// 決定キー
	if (imgr->getConfigKeyState(VK::START, false)) {
		// 状態遷移する
		m_taskState = State::Selected;
	}

	// 適用
	if (m_idx < 0) { m_idx = 0; }
	if (m_idx > 2) { m_idx = 2; }
	y += (m_idx - prev) * 50;
	m_gp["arrow"]->setPosition(x, y);
}