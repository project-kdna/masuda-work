#ifndef __INCLUDED_APPLICATION_H__
#define __INCLUDED_APPLICATION_H__

#include <DxLib.h>

class Application
{
public:
	Application();
	~Application();

	void initialize();
	void run();

private:
	LPCSTR mp_fontname;		//!< フォント名
};

#endif // !__INCLUDED_APPLICATION_H__
