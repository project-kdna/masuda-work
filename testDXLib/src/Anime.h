#ifndef __INCLUDED_ANIME_H__
#define __INCLUDED_ANIME_H__

class AnimeController;
class Drawable;

class FadeAnime
	: public AnimeTask
{
public:
	FadeAnime(Drawable* drawable, double speed, double alpha);
	FadeAnime(int layerIdx, double speed, double alpha);
	~FadeAnime();

	void initialize();
	bool run();

private:
	AnimeController* mp_actrl;
	Drawable* mp_drawable;
	int m_layerIdx;
};


class WaitAnime
	: public AnimeTask
{
public:
	WaitAnime(double speed);
	~WaitAnime();

	void initialize();
	bool run();

private:
	AnimeController* mp_actrl;
};

#endif // !__INCLUDED_ANIME_H__
