#include "DemoTask.h"

/*! @brief コンストラクタ */
DemoTask::DemoTask() {}

/*! @brief デストラクタ */
DemoTask::~DemoTask() {}

/*! @brief 初期化 */
void DemoTask::initialize()
{
	// 画像の読み込み
	m_gp.setInitAlpha(0.0);
	m_gp.create("demo", "data/demo.png");

	// アニメーションの追加
	m_amgr.append(new FadeAnime(m_gp["demo"], 500.0, 255.0));
	m_amgr.append(new WaitAnime(1000.0));
	m_amgr.append(new FadeAnime(m_gp["demo"], 500.0, 0.0));
}

/*! @brief タイトル前のデモを表示する */
int DemoTask::run()
{
	DrawManager* dmgr = DrawManager::getInstance();

	// デモアニメーション
	if (m_amgr.run() == true) {
		// デモアニメーション終了時
		// タイトルタスクに状態遷移する
		TaskManager::getInstance()->append(new TitleTask());
		TaskManager::getInstance()->remove(this);
	}
	// 描画
	dmgr->append(m_gp["demo"], 0);

	return EVT::NONE;
}