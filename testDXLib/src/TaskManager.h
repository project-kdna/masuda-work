#ifndef __INCLUDED_TASKMANAGER_H__
#define __INCLUDED_TASKMANAGER_H__

#include "Task.h"
#include <list>
#include <string>

/*! @brief タスク管理クラス
	@details このクラスを通して，タスクを実行させる．
	タスクの登録にはappend()，登録タスクの実行にはrun()を呼び出す．
*/
class TaskManager
{
private:
	TaskManager();
	TaskManager(const TaskManager& c);				// コピーコンストラクタ禁止(未定義->呼び出したらリンカエラー)
	TaskManager& operator =(const TaskManager& c);	// コピーコンストラクタ禁止(未定義->呼び出したらリンカエラー)

public:
	~TaskManager();

	static TaskManager* getInstance() {
		if (!mp_inst) { mp_inst = new TaskManager(); }
		return mp_inst;
	}
	static void deleteInstance() {
		delete mp_inst;
		mp_inst = 0;
	}

	void append(Task* task, const std::string& name, int priority, bool isAutoDelete = true);
	void remove(Task* task);
	void remove(int minPriority, int maxPriority);
	void clear();
	
	virtual int run();

	/*! @brief タスクを探索し，探索結果を返す
		@param[in] name 探索するタスクの識別名
		@return 探索結果のタスク．なければnullptrを返す．
	*/
	template <typename T>
	Task* find(const std::string& name)
	{
		// オブジェクトを名前で探索する
		std::list<Task*>::iterator it;
		for (it = mp_tasks.begin(); it != mp_tasks.end(); ++it)
		{
			if ((*it)->getName() == name) {
				return dynamic_cast<T*>(*it);
			}
		}
		// 見つからない場合
		return nullptr;
	}

private:
	std::list<Task*> mp_tasks;				//!< タスク
	std::list<Task*>::iterator m_it;		//!< タスクイテレータ
	bool m_isIteratorMoved;					//!< イテレータ先を削除したか

private:
	static TaskManager* mp_inst;			//!< Taskインスタンス
};


#endif // !__INCLUDED_TASKMANAGER_H__
