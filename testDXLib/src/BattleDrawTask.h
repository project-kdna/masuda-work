#ifndef __INCLUDED_BATTLEDRAWTASK_H__
#define __INCLUDED_BATTLEDRAWTASK_H__

#include "Task.h"
#include "FieldBox.h"
#include "Animator.h"

namespace kdna {
class FieldBox;
class Text;
}

class BattleDrawTask
	: public Task
{
public:
	~BattleDrawTask();

	void initialize();
	int run();

private:
	kdna::FieldBox m_fb;
	kdna::Text* m_text;
	kdna::Animator m_animator;
};

#endif // !__INCLUDED_BATTLEDRAWTASK_H__
