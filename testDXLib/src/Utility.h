#ifndef __INCLUDED_UTILITY_H__
#define __INCLUDED_UTILITY_H__

class util
{
public:
	
	static void cvtColorToRGB(int hexColor, int& r, int& g, int& b)
	{
		r = (hexColor >> 16) & 0xFF;
		g = (hexColor >> 8) & 0xFF;
		b = (hexColor & 0xFF);
	}

	static void cvtColorToHex(int r, int g, int b, int& hexColor)
	{
		hexColor = (r << 16 | g << 8 | b);
	}

private:
	util();
};




#endif // !__INCLUDED_UTILITY_H__
