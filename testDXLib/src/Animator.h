#ifndef __INCLUDED_ANIMATOR_H__
#define __INCLUDED_ANIMATOR_H__

#include <list>

namespace kdna {
class Animation;

class Animator
{
public:
	Animator();
	virtual ~Animator();

	void add(Animation* anime);
	void remove(Animation* anime);
	void clear();

	void start();
	void reset();

	void setIsAutoDelete(bool isAutoDelete);
	bool getHasEnded();
	bool getIsAutoDelete();

private:
	std::list<Animation*> mp_animes;
	typedef std::list<Animation*>::iterator Itr;
	Itr m_it;
	bool m_hasEnded;
	bool m_isAutoDelete;
};

inline void Animator::setIsAutoDelete(bool isAutoDelete) {
	m_isAutoDelete = isAutoDelete;
}
inline bool Animator::getHasEnded() {
	return m_hasEnded;
}
inline bool Animator::getIsAutoDelete() {
	return m_isAutoDelete;
}

}

#endif // !__INCLUDED_ANIMATOR_H__
