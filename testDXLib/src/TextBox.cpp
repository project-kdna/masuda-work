#include "TextBox.h"

namespace kdna {

/*! @brief コンストラクタ */
TextBox::TextBox()
{
	setFont(NULL, -1, -1);
	initialize();
}

/*! @brief コンストラクタ */
TextBox::TextBox(char* fontname, int size, int thick)
{
	setFont(fontname, size, thick);
	initialize();
}

/*! @brief デストラクタ */
TextBox::~TextBox() {}

/*! @brief 初期化処理 */
void TextBox::initialize()
{
}

/*! @brief テキストボックスを生成 */
void TextBox::create(int x, int y, int width, int height)
{
	setPosition(x, y);
	setSize(width, height);
}


/*! @brief 描画する文字を設定する */
void TextBox::setDrawString(char* str)
{
	// 初期化
	m_sBuffer.str("");
	m_sBuffer.clear();
	// 文字列の登録
	m_sBuffer << str;
}

/*! @brief 表示するフォントを設定する */
void TextBox::setFont(char* fontname, int size, int thick)
{
	if (thick < 0 || thick > 9) { thick = -1; }
	// 保持フォントを削除
	if (m_fontHandle != -1) {
		DeleteFontToHandle(m_fontHandle);
	}
	// フォントの生成
	m_fontHandle = CreateFontToHandle(fontname, size, thick, DX_FONTTYPE_NORMAL);
}

/*! @brief 描画 */
void TextBox::draw()
{
	// 領域からはみ出しそうなときに改行させる
	// (文字数制限で改行させない)
	// 日本語は"あ"英語は"a"を基準とした横幅
	// 文字の装飾はmarkdown的な記号で制御する

}

/*! @brief Shift-JISの部分文字列を取得 */
void TextBox::substr(char* str, char* sub, int start, int len)
{
	int maxLen = strlen(str);		// 文字数
	int idx = 0;					// 参照文字インデックス
	// バイトを考慮したstartの位置を求める
	while (idx < start && str[idx] != '\0')
	{
		unsigned char c = str[idx];
		// SHIFT-JISの日本語判定
		if ((c >= 0x81 && c <= 0x9F) || (c >= 0xE0 && c <= 0xFC)) {
			// 日本語は2バイト文字なので2つ進める
			idx += 2;
			// startの位置を修正する
			start++;
		}
		else {
			idx++;
		}
	}
	if (start >= maxLen) {
		sub[0] = '\0';
		return;
	}
	// 部分文字列を取得する
	idx = 0;
	while (idx < len && str[start + idx] != '\0')
	{
		unsigned char c = str[start + idx];
		// SHIFT-JISの日本語判定
		if ((c >= 0x81 && c <= 0x9F) || (c >= 0xE0 && c <= 0xFC)) {
			unsigned char c1 = str[start + idx + 1];
			sub[idx] = c;
			sub[idx + 1] = c1;
			// 日本語は2バイト文字なので2つ進める
			idx += 2;
			// lenを修正する
			len++;
		}
		else {
			sub[idx] = c;
			idx++;
		}
	}
	// '\0'の挿入
	sub[len] = '\0';
}

}