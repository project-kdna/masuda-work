/*! @file Task.h 
	@brief タスククラス
	@author Shohei Masuda
*/

#ifndef __INCLUDED_TASK_H__
#define __INCLUDED_TASK_H__

#include <vector>
#include <list>
#include <string>
#include <cassert>
#include <Windows.h>

class TaskManager;

/*! @brief タスク基底クラス 
	@details 新しいタスクを生成するときは，必ずこのクラスを継承させること．
			 initialize()で初期化され，run()が実行される． 
*/
class Task
{
public:
	Task();
	~Task();

	virtual void initialize() = 0;
	virtual int run() = 0;

	void setIsAutoDelete(bool isAutoDelete) { m_isAutoDelete = isAutoDelete; }
	void setIsValid(bool isValid)			{ m_isValid = isValid; }
	void setName(const std::string& name)			{ m_name =  name; }
	void setPriority(int priority)			{ m_priority = priority; }

	std::string& getName()	{ return m_name; }
	bool getIsAutoDelete()	{ return m_isAutoDelete; }
	int getPriority()		{ return m_priority; }
	bool getIsValid()		{ return m_isValid; }

protected:
	std::string m_name;			//!< タスク名
	int m_priority;				//!< 優先度
	bool m_isAutoDelete;		//!< タスク完了時に自動的にdeleteするか
	bool m_isValid;				//!< タスクが有効かどうか
};


#endif // !__INCLUDED_TASK_H__
