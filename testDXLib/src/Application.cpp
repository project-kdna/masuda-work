#include "Application.h"
#include "RootScene.h"
#include "InputManager.h"
#include "DrawManager.h"
#include "TaskManager.h"
#include "BattleDrawTask.h"
using namespace kdna;


/*! @brief コンストラクタ */
Application::Application() {}

/*! @brief デストラクタ */
Application::~Application()
{
	printf("正常終了しました！\n");

	
	// フォントの開放
	if (RemoveFontResourceEx(mp_fontname, FR_PRIVATE, NULL) < 1) {
		MessageBox(NULL, "フォント開放失敗", "", MB_OK);
	}

	// DxLibの終了
	DxLib_End();
}

void Application::initialize()
{
	// ウィンドウモードの設定
	ChangeWindowMode(true);
	DxLib_Init();
	SetDrawScreen(DX_SCREEN_BACK);

	
	// フォントの読み込み
	mp_fontname = "data/ueten/ueten-1m-medium.ttf";
	if (AddFontResourceEx(mp_fontname, FR_PRIVATE, NULL) < 1) {
		MessageBox(NULL, "フォント読み込み失敗", "", MB_OK);
	}

	// 描画マネージャの初期化
	DrawManager* dmgr = DrawManager::getInstance();

	// 入力マネージャの初期化
	InputManager* idev = InputManager::getInstance();
	idev->assign(KEY_INPUT_UP, VK::UP);
	idev->assign(KEY_INPUT_DOWN, VK::DOWN);
	idev->assign(KEY_INPUT_LEFT, VK::LEFT);
	idev->assign(KEY_INPUT_RIGHT, VK::RIGHT);
	idev->assign(KEY_INPUT_RETURN, VK::START);
}

void Application::run()
{
	//RootScene scene;
	DrawManager* dmgr = DrawManager::getInstance();
	InputManager* idev = InputManager::getInstance();

	TaskManager* tmgr = TaskManager::getInstance();
	tmgr->append(new BattleDrawTask(), "BattleDrawTask", 0, true);

	// メインループ
	while (true)
	{
		if (ProcessMessage()) { break; }
		ClearDrawScreen();
		idev->updateKeyboard();

		// シーンの実行
		//if (scene.run() == nullptr) { break; }
		// タスクの実行
		if (tmgr->run() == EVT::QUIT) { break; }

		// 描画
		dmgr->draw();

		dmgr->clearAll();
		ScreenFlip();
	}
}
