#ifndef __INCLUDED_INTERPOLATOR_H__
#define __INCLUDED_INTERPOLATOR_H__

#include <vector>

namespace kdna {

class Interpolator
{
public:
	virtual ~Interpolator() {}

	virtual double getInterpolator(double t) = 0;

	static double lerp(double a, double b, double s);
};

}
#endif // !__INCLUDED_INTERPOLATOR_H__
