#include "FieldBox.h"
#include "FieldReader.h"
#include <DxLib.h>
#include <string>

namespace kdna {

FieldBox::FieldBox()
{
	mp_field = nullptr;
	m_handle = -1;
	this->initialize();
}


FieldBox::~FieldBox()
{
	delete[] mp_field;
}


void FieldBox::initialize() {}


void FieldBox::setField(unsigned char* field, int bitCount, int width, int height)
{
	// チェック
	if (mp_field != nullptr) { return; }

	// レイヤーをコピーする
	int size = width * height * (bitCount / 8);
	mp_field = new unsigned char[size];
	memcpy(mp_field, field, size);
	// 登録
	setSize(width, height);


	m_bitCount = bitCount;
}

bool FieldBox::setImageResource(int handle, int chipWidth, int chipHeight)
{
	// グラフィックのサイズを得る
	int gw, gh;
	GetGraphSize(handle, &gw, &gh);
	// 登録
	m_handle = handle;
	m_chipWidth = chipWidth;
	m_chipHeight = chipHeight;
	m_srcWidth = gw / chipWidth;
	m_srcHeight = gh / chipHeight;
	return true;
}

bool FieldBox::setFieldData(int handle, FieldReader& field, int layerIdx)
{
	this->setField(field.getLayer(layerIdx),
				   field.getBitCount(),
				   field.getWidth(),
				   field.getHeight()
				   );

	this->setImageResource(handle,
					 field.getChipWidth(),
					 field.getChipHeight()
					 );
	return true;
}


void FieldBox::draw()
{
	int x = getX(), y = getY();
	int width = getWidth(), height = getHeight();
	double alpha = getAlpha();
	int alphaInt = static_cast<int>(alpha * 255.0);

	// チェック
	if (mp_field == nullptr || m_handle == -1) { return; }
	if (alpha <= 0.0 && !getIsVisible()) { return; }

	// マップの描画
	if (alpha < 1.0) { SetDrawBlendMode(DX_BLENDMODE_ALPHA, alphaInt); }
	for (int j = 0; j < height; j++)
	{
		for (int i = 0; i < width; i++)
		{
			int srcIdx = get(i, j);
			int srcX = (srcIdx % m_srcWidth) * m_chipWidth;
			int srcY = (srcIdx / m_srcWidth) * m_chipHeight;

			// 描画
			DrawRectGraph(
				(i * m_chipWidth) + x, (j * m_chipHeight) + y,
				srcX, srcY,
				m_chipWidth, m_chipHeight,
				m_handle,
				true, false);
		}
	}
	if (alpha < 1.0) { SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0); }
}


int FieldBox::get(int x, int y) const
{
	int m_width = getWidth(), m_height = getHeight();

	// チェック
	if (x >= m_width || y >= m_height) { return -1; }

	// ポインタ移動
	unsigned char* p = mp_field + (y * m_width + x) * (m_bitCount / 8);
	// 8bitレイヤー
	if (m_bitCount == 8) { return static_cast<int>(*p); }
	// 16bitレイヤー
	if (m_bitCount == 16) {
		return static_cast<int>(*(p + 1) << 8 | (*p));
	}

	return -1;
}

}