#include "Image.h"
#include <DxLib.h>

namespace kdna {

/*! @brief コンストラクタ */
Image::Image()
{
	m_handle = -1;
}

/*! @brief デストラクタ */
Image::~Image() {}


bool Image::setGraphicHandle(int handle)
{
	// 投げる
	return this->setGraphicHandle(handle, 0, 0, 1.0);
}

bool Image::setGraphicHandle(int handle, int x, int y, double alpha)
{
	// チェック
	if (handle == -1) { return false; }
	// 画像のサイズを得る
	int w, h;
	GetGraphSize(m_handle, &w, &h);
	// 登録
	m_handle = handle;
	this->setPosition(x, y);
	this->setSize(w, h);
	this->setAlpha(alpha);

	return true;
}


/*! @brief 画像を描画 */
void Image::draw()
{
	double alpha = this->getAlpha();
	int alphaInt = static_cast<int>(alpha * 255.0);
	int x = this->getX();
	int y = this->getY();

	// 画像を描画する
	if (alpha > 0.0 && getIsVisible()) {
		if (alpha < 1.0) { SetDrawBlendMode(DX_BLENDMODE_ALPHA, alphaInt); }
		DrawGraph(x, y, m_handle, true);
		if (alpha < 1.0) { SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0); }
	}
}

}