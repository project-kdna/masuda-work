#include "FieldReader.h"


bool FieldReader::load(const char* filename)
{
	// ファイルの読み込み
	FILE *fp;
	if ((fopen_s(&fp, filename, "rb")) != 0) { return false; }

	// ファイルヘッダを読み込む
	unsigned char h[20];
	if (fread(h, sizeof(h), 1, fp) != 1) {
		printf("error: ファイルメモリ読み込み失敗\n");
		return false;
	}
	DWORD identifier =	readDWORD(&h[0]);
	DWORD size =		readDWORD(&h[4]);
	DWORD width =		readDWORD(&h[8]);
	DWORD height =		readDWORD(&h[12]);
	BYTE  chipWidth =	readBYTE(&h[16]);
	BYTE  chipHeight =	readBYTE(&h[17]);
	BYTE  layerCount =	readBYTE(&h[18]);
	BYTE  bitCount =	readBYTE(&h[19]);

	// 識別子の確認
	if (memcmp(&identifier, "FMF_", 4) != 0) {
		printf("error: ファイルの種類が違う\n");
		return false;
	}

	//読み込んだ値をプリミティブ型に変換
	m_fieldSize =	static_cast<int>(size);
	m_width =		static_cast<int>(width);
	m_height =		static_cast<int>(height);
	m_chipWidth =	static_cast<int>(chipWidth);
	m_chipHeight =	static_cast<int>(chipHeight);
	m_layerCount =	static_cast<int>(layerCount);
	m_bitCount =	static_cast<int>(bitCount);

	// デバッグ用
	OutputDebugStringF("Field FileSize:   %d\n", m_fieldSize);
	OutputDebugStringF("Field Width:      %d\n", m_width);
	OutputDebugStringF("Field Height:     %d\n", m_height);
	OutputDebugStringF("Field ChipWidth:  %d\n", m_chipWidth);
	OutputDebugStringF("Field ChipHEight: %d\n", m_chipHeight);
	OutputDebugStringF("Field LayerCount: %d\n", m_layerCount);
	OutputDebugStringF("Field BitCount:   %d\n", m_bitCount);

	// レイヤーデータの読み込み
	BYTE* layer = new BYTE[m_fieldSize];
	if (fread(layer, m_fieldSize, 1, fp) != 1) {
		printf("error: レイヤーメモリ読み込み失敗\n");
		return false;
	}
	// BYTE -> unsigned charに変換
	mp_layer = reinterpret_cast<unsigned char*>(layer);

	// 開放
	fclose(fp);
	return true;
}


unsigned char* FieldReader::getLayer(int layerIdx) const 
{
	// メモリチェック
	if (mp_layer == nullptr || layerIdx >= m_layerCount) {
		return nullptr;
	}

	int size = m_width * m_height * (m_bitCount / 8) * layerIdx;
	// 指定レイヤデータのアドレスは計算で求められる
	return mp_layer + size;
}