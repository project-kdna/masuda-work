#include "Diagram.h"
#include <DxLib.h>
#include "Utility.h"

namespace kdna {

/*! @brief コンストラクタ */
Rectangle::Rectangle()
{
	m_color = 0x000000;
	m_isFill = true;
}

/*! @brief コンストラクタ */

Rectangle::Rectangle(int x, int y, int width, int height, int color, bool isFill)
{
	setPosition(x, y);
	setSize(width, height);
	m_color = color;
	m_isFill = isFill;
}

/*! @brief デストラクタ */
Rectangle::~Rectangle() {}

/*! @brief 矩形を描画 */
void Rectangle::draw()
{
	double alpha = this->getAlpha();
	int alphaInt = static_cast<int>(alpha * 255.0);
	int x = this->getX();
	int y = this->getY();
	int width = this->getWidth();
	int height = this->getHeight();

	int r, g, b;
	util::cvtColorToRGB(m_color, r, g, b);

	// 矩形を描画する
	if (alpha > 0.0 && getIsVisible()) {
		if (alpha < 1.0) { SetDrawBlendMode(DX_BLENDMODE_ALPHA, alphaInt); }

		DrawBox(x, y, x + width, y + height, GetColor(r, g, b), m_isFill);

		if (alpha < 1.0) { SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0); }
	}
}

}