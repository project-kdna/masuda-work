#include "WaitAnimation.h"

namespace kdna {

WaitAnimation::WaitAnimation()
{
	setDuration(0);
	setInterpolator(&m_li);
}

WaitAnimation::WaitAnimation(int msec)
{
	setDuration(msec);
	setInterpolator(&m_li);
}

void WaitAnimation::start()
{
	setHasEnded(false);

	double normalizedTime = getNormalizedTime();
	if (normalizedTime == 1.0) {
		setHasEnded(true);
	}
}

void WaitAnimation::reset()
{

}

}