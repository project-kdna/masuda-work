#ifndef __INCLUDED_GRAPHICIMAGE_H__
#define __INCLUDED_GRAPHICIMAGE_H__

#include "Drawable.h"

namespace kdna {

/*! @brief 画像クラス */
class Image
	:public Drawable
{
public:
	Image();
	virtual ~Image();

	bool setGraphicHandle(int handle);
	bool setGraphicHandle(int handle, int x, int y, double alpha);

	bool isEmpty() const;

	// override
	void draw();

private:
	int m_handle;				//!< 画像ハンドル
};

inline bool Image::isEmpty() const { return (m_handle == -1); }

}
#endif // !__INCLUDED_GRAPHICIMAGE_H__
