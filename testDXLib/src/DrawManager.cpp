#include "DrawManager.h"
#include "Drawable.h"
#include <DxLib.h>

namespace kdna {

DrawManager::DrawManager()
{
	// 決められた数を確保しておき，nullptrで初期化
	mp_layers.resize(MAX_LAYER_NUM, nullptr);
}

DrawManager::~DrawManager() {}

/*!
	@brief 描画可能オブジェクトを描画リストに追加します．

	指定したレイヤーに対して，描画可能オブジェクトを描画するように登録します．

	@param[in] drawable 描画可能オブジェクト
	@param[in] layerIdx レイヤー番号
*/
void DrawManager::add(Drawable* drawable, int layerIdx)
{
	if (mp_layers.at(layerIdx) == nullptr) {
		// 新しくレイヤーを生成する
		mp_layers[layerIdx] = new Layer();
		mp_layers[layerIdx]->setAlpha(1.0);
		mp_layers[layerIdx]->setIsVisible(true);
	}

	// 描画予定項目の追加
	mp_layers[layerIdx]->add(drawable);
}

/*!
	@brief 内部に持つレイヤーを指定レイヤーで置き換えます

	置き換え元レイヤーの内容を全てコピーします．
	そのため，置き換え先レイヤーの内容は全て除去されます．

	@param[in] layer レイヤー
	@param[in] layerIdx レイヤー番号
	*/
void DrawManager::replaceLayer(Layer& layer, int layerIdx)
{
	// TODO レイヤーを置き換える
}

/*!
	@brief 描画可能オブジェクトをレイヤーの描画リストから除去します

	指定されたオブジェクトが見つからない場合は，何もしません．

	@param[in] drawable 描画可能オブジェクト
	@param[in] layerIdx レイヤー番号
	*/
void DrawManager::remove(Drawable* drawable, int layerIdx)
{
	if (mp_layers.at(layerIdx) != nullptr) {
		// レイヤーに除去を依頼する
		mp_layers.at(layerIdx)->remove(drawable);
	}
}

/*!
	@brief 指定されたレイヤーの描画リストの内容を全て除去します．

	@param[in] layerIdx レイヤー番号
	*/
void DrawManager::clear(int layerIdx)
{
	if (mp_layers.at(layerIdx) != nullptr) {
		mp_layers.at(layerIdx)->clear();
	}
}

/*!
	@brief 全てのレイヤーの描画リストの内容を全て除去します．
	*/

void DrawManager::clearAll()
{
	for (Itr it = mp_layers.begin(); it != mp_layers.end(); ++it)
	{
		// レイヤーにお願いする
		if ((*it) != nullptr) {
			(*it)->clear();
		}
	}
}

/*!
	@brief 追加された情報に基づいて描画を開始します．

	描画可能オブジェクトは，描画後にリストから除去されます．

	*/
void DrawManager::draw()
{
	for (Itr it = mp_layers.begin(); it != mp_layers.end(); ++it)
	{
		if ((*it) != nullptr) {
			(*it)->draw();
		}
	}
	clearAll();
}

}