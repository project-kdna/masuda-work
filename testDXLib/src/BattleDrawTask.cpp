#include "BattleDrawTask.h"
#include "DrawManager.h"
#include "FieldReader.h"
#include "Text.h"
#include "AlphaAnimation.h"
#include "WaitAnimation.h"
#include <DxLib.h>

BattleDrawTask::~BattleDrawTask()
{
	delete m_text;
}

void BattleDrawTask::initialize()
{
	// マップをロードする
	FieldReader fr;
	fr.load("data/sample/15x8.fmf");
	// 画像をロードする
	int handle = LoadGraph("data/sample/empire-3.bmp");
	int handle2 = LoadGraph("data/player.bmp");

	// リソースの登録
	m_text = new kdna::Text(0, 400, "ああああああああああああああああああ");

	// 設定する
	m_fb.setPosition(0, 0);
	m_fb.setFieldData(handle, fr, 0);
	m_fb.setAlpha(1.0);
	m_fb.setIsVisible(true);

	m_animator.add(new kdna::AlphaAnimation(m_text, 0.0, 1.0, 1500));
	m_animator.add(new kdna::WaitAnimation(1000));
	m_animator.add(new kdna::AlphaAnimation(m_text, 1.0, 0.0, 1500));
	m_animator.setIsAutoDelete(true);
}


int BattleDrawTask::run()
{
	kdna::DrawManager* dmgr = kdna::DrawManager::getInstance();

	m_animator.start();

	dmgr->add(&m_fb, 0);
	dmgr->add(m_text, 2);

	return 0;
}