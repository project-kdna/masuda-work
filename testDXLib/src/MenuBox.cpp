/*! @file MenuBox.cpp
	@brief メニューウィンドウ 
	@author Shohei Masuda
*/

#include "MenuBox.h"
#include "DrawManager.h"
#include <DxLib.h>

namespace kdna {

/*!
	@brief
コンストラクタ */
MenuBox::MenuBox()
{
	this->initialize();
}

/*!
	@brief デストラクタ
*/
MenuBox::~MenuBox() {}

void MenuBox::initialize()
{
	// 値の初期化
	m_chipWidth = m_chipHeight = 0;
	mp_handles[0] = -1;
	m_r = m_g = m_b = 0;
}

/*!
	@brief ポップアップウィンドウ枠に使用する画像ハンドルを指定
	@details 画像ファイルは，画像チップを指定したフォーマットで並べて置かなければならない
	@param[in] divHandles RPGツクール形式の枠画像ファイルのハンドル．サイズは16
	@return 画像読み込み成功時にtrue
*/
void MenuBox::setImage(int divHandles[], int chipWidth, int chipHeight)
{
	// チップサイズ設定
	m_chipWidth = chipWidth;
	m_chipHeight = chipHeight;
	// ハンドルの整理
	mp_handles[0] = divHandles[0];			// 左上
	mp_handles[1] = divHandles[3];			// 右上
	mp_handles[2] = divHandles[12];			// 左下
	mp_handles[3] = divHandles[15];			// 右下
	mp_handles[4] = divHandles[1];			// 上
	mp_handles[5] = divHandles[13];			// 下
	mp_handles[6] = divHandles[4];			// 左
	mp_handles[7] = divHandles[7];			// 右
}

/*! @brief ポップアップウィンドウ枠に使用すつ画像ファイルを読み込む
	@details 画像ファイルは，画像チップを指定したフォーマットで並べて置かなければならない
	@return 画像読み込み成功時にtrue
	*/
bool MenuBox::loadImage(const char* filename, int chipWidth, int chipHeight)
{
	if (mp_handles[0] == -1) {
		// チップサイズ設定
		m_chipWidth = chipWidth;
		m_chipHeight = chipHeight;
		// 画像の読み込み（RGBツクールフォーマットの場合）
		int handles[16];
		if (LoadDivGraph(filename, 16, 4, 4, m_chipWidth, m_chipHeight, handles) == -1) {
			return false;
		}
		// 画像の指定
		this->setImage(handles, chipWidth, chipHeight);
	}

	return true;
}


/*! @brief ウィンドウの描画 */
void MenuBox::draw()
{
	int x = getX(), y = getY(), width = getWidth(), height = getHeight();
	double alpha = getAlpha();
	int alphaInt = static_cast<int>(alpha * 255.0);

	const int mapWidth = width / m_chipWidth;
	const int mapHeight = height / m_chipHeight;
	int w = x + width - m_chipWidth;
	int h = y + height - m_chipHeight;
	int chw = m_chipWidth / 4;
	int chh = m_chipHeight / 4;

	if (alpha > 0.0 && mp_handles[0] != -1) {
		if (alpha < 1.0) { SetDrawBlendMode(DX_BLENDMODE_ALPHA, alphaInt  / 2); }

		// 背景の描画
		DrawBox(x + chw, y + chh, x + width - chw, y + height - chh, GetColor(0, 0, 0), true);

		if (alpha < 1.0) { SetDrawBlendMode(DX_BLENDMODE_ALPHA, alphaInt); }

		// 隅の描画
		DrawGraph(x, y, mp_handles[0], true);				// 左上
		DrawGraph(w, y, mp_handles[1], true);					// 右上
		DrawGraph(x, h, mp_handles[2], true);					// 左下
		DrawGraph(w, h, mp_handles[3], true);					// 右下
		// 枠を描画する
		for (int i = 1; i < mapWidth - 1; i++)
		{
			int x2 = x + (i * m_chipWidth);
			DrawGraph(x2, y, mp_handles[4], true);				// 上
			DrawGraph(x2, h, mp_handles[5], true);				// 下
		}
		for (int i = 1; i < mapHeight - 1; i++)
		{
			int y2 = y + (i * m_chipHeight);
			DrawGraph(x, y2, mp_handles[6], true);				// 左
			DrawGraph(w, y2, mp_handles[7], true);				// 右
		}

		if (alpha < 1.0) { SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0); }
	}
}


}