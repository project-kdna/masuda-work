#include "common.h"
#include "Application.h"

int WINAPI WinMain(HINSTANCE, HINSTANCE, LPTSTR, int)
{
#ifdef _DEBUG
	createConsoleWindow();
#endif

	// アプリケーションの実行
	Application* app = new Application();
	app->initialize();
	app->run();
	delete app;
	

#ifdef _DEBUG
	closeConsoleWindow();
#endif

	return 0;
}