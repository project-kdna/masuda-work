#ifndef __INCLUDED_MENUBOX_H__
#define __INCLUDED_MENUBOX_H__

#include "Drawable.h"

namespace kdna {

class MenuBox
	: public Drawable
{
public:
	MenuBox();
	~MenuBox();

	void initialize();
	bool loadImage(const char* filename, int chipWidth, int chipHeight);
	void setImage(int divHandles[], int chipWidth, int chipHeight);
	void setRect(int x, int y, int width, int height)
	{
		this->setPosition(x, y);
		this->setSize((width / m_chipWidth) * m_chipWidth, (height / m_chipHeight) * m_chipHeight);
	}
	void setColor(int r, int g, int b) { m_r = r; m_g = g; m_b = b; }

	// Override
	void draw();

private:
	int mp_handles[8];						//<! 枠画像のハンドル
	int m_chipWidth;						//<! マップチップのサイズ
	int m_chipHeight;						//<! マップチップのサイズ
	int m_r, m_g, m_b;						//<! 背景色
};

}
#endif // !__INCLUDED_MENUBOX_H__
