#include "Scene.h"

namespace kdna {

/*! @brief コンストラクタ */
Scene::Scene(Scene* parent)
{
	mp_parent = parent;
	m_isValid = false;
}

/*! @brief デストラクタ */
Scene::~Scene() {}

/*!@brief シーンを有効にする */
void Scene::validate()
{
	// 初期化する
	initialize();
	m_isValid = true;
}

/*! @brief シーンを開放する（無効にする） */
void Scene::release()
{
	m_isValid = false;
}

/*! @brief シーンを削除する */
void Scene::destroy()
{
	// 子供がいるなら
	if (!mp_childs.empty())
	{
		// 子供を全員殺す
		std::vector<Scene*>::iterator it;
		for (it = mp_childs.begin(); it != mp_childs.end(); ++it)
		{
			delete (*it);
		}
	}
}


/*!	@brief イベントを親シーンに渡し，次に実行するシーンを返す
	@param[in] caller 関数呼出し元のポインタ(通常はthis)
	@param[in] message 親シーンにシーン遷移を判断してもらうためのイベント
	@return 次に実行するシーン
	*/
Scene* Scene::receiveMessage(Scene* caller, int message)
{
	// 呼び出し元シーンの親を取得
	Scene* parent = caller->getParent();
	// イベントを処理する
	Scene* next = this->processMessage(caller, message);

	// イベントが処理できなかった場合
	if (next == NULL) {
		// 呼び出し元の親なし or 呼び出し元の親が自分の場合，子が親の関数を呼んでいると判断
		if (mp_parent != NULL && parent == this) {
			// 親にイベントを投げる
			next = mp_parent->receiveMessage(this, message);
		}
		else {
			// 子供全員にイベントを投げる
			std::vector<Scene*>::iterator it;
			for (it = mp_childs.begin(); it != mp_childs.end(); it++)
			{
				Scene* child = (*it);

				// 呼び出し元以外の子供に聞いて回る
				if (caller != child) {
					next = child->receiveMessage(child, message);
					// 誰か知っていたらbreak
					// NULLなら探索失敗
					if (next != NULL) { break; }
				}
			}
		}
	}

	return next;
}

/*!	@brief receiveMessage()内でイベントを処理する関数
	子供を持つシーンは必ず実装すること

	@param[in] caller 関数呼出し元のポインタ(通常はthis)
	@param[in] message シーン遷移を判断するためのイベント
	@return イベントに対するシーン(NULL: イベント処理不可)
	*/
Scene* Scene::processMessage(Scene* caller, int message)
{
	Scene* next = 0;

	switch (message)
	{
		case EVT::NONE:
			next = caller;
			break;

		default:
			next = NULL;
			break;
	}

	return next;
}

}