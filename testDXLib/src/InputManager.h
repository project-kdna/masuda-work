#ifndef __INCLUDED_INPUTDEVICE_H__
#define __INCLUDED_INPUTDEVICE_H__

#include "common.h"

#include <vector>
#include <DxLib.h>

#define KEY_INPUT_MAX 256

namespace kdna {

/*! @brief 仮想ボタン */
struct VK
{
	enum {
		NONE,
		A,
		B,
		X,
		Y,
		START,
		SELECT,
		UP,
		DOWN,
		LEFT,
		RIGHT,
		LENGTH
	};
};

/*! @brief 仮想マウス */
struct VM
{
	enum {
		LEFT,
		MIDDLE,
		RIGHT,
		LENGTH
	};
};

namespace {;

class ConfigKeyMap
{
public:

	ConfigKeyMap();
	virtual ~ConfigKeyMap();

	void initialize();

	void assign(int inputKey, int configKey);
	bool assignFromFile(char* filename);

	int getKeyState(int configKey);

private:
	int* mp_configKeyMap;		//!< キーコンフィグマップ
	int* mp_configMouseMap;		//!< マウスコンフィグマップ
};

}

/*! @brief 入力デバイス管理クラス 

	入力デバイス(マウス、キーボード、ゲームパッドなど)の入力を監視するシングルトンクラス。
	configKeyMapを登録することで、キーコンフィグにも対応する。

*/
class InputManager
{
public:

	virtual ~InputManager();

	static InputManager* getInstance()
	{
		if (!mp_inst) { mp_inst = new InputManager(); }
		return mp_inst;
	}
	static void deleteInstance()
	{
		delete mp_inst; mp_inst = 0;
	}

	void initialize();

	void assign(int inputKey, int configKey);
	bool assignFromFile(char* filename);
	
	int getKeyState(int inputKey);
	int getConfigKeyState(int configKey, bool isTrigger = true);

	void updateKeyboard();
	void updateMouse();
	void updateGamepad();
	void updateAll();

private:
	ConfigKeyMap* mp_configKeyMap;		//!< キーマップクラス

	int* mp_keyCounter;		//!< DXLib: 長押しの検出
	int m_mx;				//!< DxLib: マウスX座標
	int m_my;				//!< DxLib: マウスY座標
	int m_mInput;			//!< DxLib: マウス入力

	static InputManager* mp_inst;		//!< インスタンス

	InputManager();
	InputManager(const InputManager& c);				// コピーコンストラクタ禁止(未定義->呼び出したらリンカエラー)
	InputManager& operator =(const InputManager& c);	// コピーコンストラクタ禁止(未定義->呼び出したらリンカエラー)

};


}
#endif // !__INCLUDED_INPUTDEVICE_H__