#ifndef __INCLUDED_LINEARINTERPOLATOR_H__
#define __INCLUDED_LINEARINTERPOLATOR_H__

#include "Interpolator.h"

namespace kdna {

class LinearInterpolator
	: public Interpolator
{
public:
	LinearInterpolator() {}
	~LinearInterpolator() {}

	double getInterpolator(double t);
};

/*!
	@brief 引数をパラメータとした補間関数の結果を返します．

	入力をt，補間関数をv(t)とすると，v(t)=tとなります．
	tは0.0から1.0の値を取ることを注意して下さい．

	@param[in] t 媒介変数．0.0は開始を表し，1.0で終了します．
	@return 補間値．線形補間であるため，必ず0.0から1.0までの値になります．
	*/
inline double LinearInterpolator::getInterpolator(double t) {
	return t;
}

}
#endif // !__INCLUDED_LINEARINTERPOLATOR_H__
