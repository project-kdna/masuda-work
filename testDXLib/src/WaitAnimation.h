#ifndef __INCLUDED_WAITANIMATION_H__
#define __INCLUDED_WAITANIMATION_H__

#include "Animation.h"
#include "LinearInterpolator.h"

namespace kdna {

class WaitAnimation
	: public Animation
{
public:
	WaitAnimation();
	WaitAnimation(int msec);

	void start();	// override
	void reset();	// override

private:
	LinearInterpolator m_li;
};

}
#endif // !__INCLUDED_WAITANIMATION_H__
