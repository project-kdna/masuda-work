#ifndef __INCLUDED_FIELDREADER_H__
#define __INCLUDED_FIELDREADER_H__

#include "DebugUtility.h"
#include <iostream>
#include <Windows.h>

class FieldReader
{
public:

	bool load(const char* filename);
	bool isOpen() { return (mp_layer != nullptr); }

	unsigned char* getLayer(int layerIdx) const;
	int getWidth()		{ return m_width; }
	int getHeight()		{ return m_height; }
	int getChipWidth()	{ return m_chipWidth; }
	int getChipHeight() { return m_chipHeight; }
	int getLayerCount() { return m_layerCount; }
	int getBitCount()	{ return m_bitCount; }

private:

	DWORD readDWORD(const unsigned char* p) { return (*(p + 3) << 24 | *(p + 2) << 16 | *(p + 1) << 8 | (*p)); }
	BYTE  readBYTE(const unsigned char* p) { return (*p); }

private:
	unsigned char* mp_layer;		//!< レイヤー
	int m_fieldSize;				//!< フィールドのサイズ
	int m_width;					//!< フィールドの横幅（チップ単位）
	int m_height;					//!< フィールドの縦幅（チップ単位）
	int m_chipWidth;				//!< チップの横幅
	int m_chipHeight;				//!< チップの縦幅
	int m_layerCount;				//!< レイヤー数
	int m_bitCount;					//!< チップ一つが表現されているビット数
};

#endif // !__INCLUDED_FIELDREADER_H__
