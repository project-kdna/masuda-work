#ifndef __INCLUDED_ROOTSCENE_H__
#define __INCLUDED_ROOTSCENE_H__

#include "common.h"
#include "DebugUtility.h"
#include "Scene.h"

struct SC {
	enum ROOT {
		TITLE,
		LENGTH
	};
};

class RootScene :
	public Scene
{
public:
	RootScene();
	virtual ~RootScene();

	void initialize();
	Scene* run();
	void draw();

	Scene* processMessage(Scene* caller, int message);

private:
	Scene* mp_scene;	// 現在更新しているシーン
};

#endif // !__INCLUDED_ROOTSCENE_H__
