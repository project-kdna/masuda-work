#ifndef __INCLUDED_SAVELOADTASK_H__
#define __INCLUDED_SAVELOADTASK_H__

#include "Task.h"
#include "DrawManager.h"
#include "InputManager.h"
#include "Provider.h"
#include "MenuBox.h"

class SaveLoadTask
	: public Task
{
public:
	SaveLoadTask();
	~SaveLoadTask();

	void initialize();
	int run();

private:
	GraphProvider m_gp;
	TextProvider m_tp;
	MenuBox m_leftBox;
	MenuBox m_rightBox;
};

#endif // !__INCLUDED_SAVELOADTASK_H__
