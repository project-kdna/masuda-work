#include "TitleScene.h"

/*! @brief コンストラクタ */
TitleScene::TitleScene(Scene* parent) : Scene(parent)
{
	// シーンを有効にする
	validate();
}

/*! @brief デストラクタ */
TitleScene::~TitleScene()
{
	// 子供シーンを全て削除する
	destroy();
}


/*! @brief 初期化する */
void TitleScene::initialize()
{
	printf("TitleScene: 生成\n");

	if (mp_chara == nullptr) {
		//mp_chara = new GraphicImage(LoadGraph("data/player.bmp"), 255.0, 15, 15);
	}

	// タスクの登録
	TaskManager* tmgr = TaskManager::getInstance();
	tmgr->append(new DemoTask());
}

/*! @brief シーンを実行する */
Scene* TitleScene::run()
{
	int message = EVT::NONE;

	// タスクの実行
	message = TaskManager::getInstance()->run();

	return mp_parent->receiveMessage(this, message);
}

/*! @brief 描画する */
void TitleScene::draw()
{
	//DrawManager* dmgr = DrawManager::getInstance();
	//dmgr->draw();
}