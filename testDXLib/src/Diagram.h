#ifndef __INCLUDED_DIAGRAMIMAGE_H__
#define __INCLUDED_DIAGRAMIMAGE_H__

#include "Drawable.h"

namespace kdna {

/*! @brief 矩形クラス */
class Rectangle
	: public Drawable
{
public:
	Rectangle();
	Rectangle(int x, int y, int width, int height, int color, bool isFill = true);
	~Rectangle();

	void draw();

private:
	int m_color;				//!< 矩形の色
	bool m_isFill;				//!< 矩形を塗りつぶすかどうか
};

}
#endif // !__INCLUDED_DIAGRAMIMAGE_H__
