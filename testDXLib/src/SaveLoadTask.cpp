#include "SaveLoadTask.h"

SaveLoadTask::SaveLoadTask() {}


SaveLoadTask::~SaveLoadTask() {}


void SaveLoadTask::initialize()
{
	//int fontHandle = CreateFontToHandle("Ueten 1m medium", 48, 0, DX_FONTTYPE_ANTIALIASING);
	//int fontHandle = CreateFontToHandle("ＭＳ　ゴシック", 18, 1);
	//m_tp.setInitFont(fontHandle);
	m_tp.setInitColor(255, 255, 255);
	m_tp.setInitAlpha(255.0);
	m_tp.create("str", "ただいま\nSAVELOAD\nた\nす\nく");

	int handles[16];
	LoadDivGraph("data/Window_a_00_.png", 16, 4, 4, 16, 16, handles);
	
	m_leftBox.setImage(handles, 16, 16);
	m_leftBox.setRect(0, 0, 16 * 12, 480);
	m_leftBox.setAlpha(254.0);

	m_rightBox.setImage(handles, 16, 16);
	m_rightBox.setRect(16 * 12, 0, 16 * 23, 480);
	m_rightBox.setAlpha(254.0);
}

int SaveLoadTask::run()
{
	DrawManager* dmgr = DrawManager::getInstance();
	InputManager* imgr = InputManager::getInstance();

	dmgr->append(&m_leftBox, 20);
	dmgr->append(&m_rightBox, 20);
	dmgr->append(m_tp["str"], 30);

	return EVT::NONE;
}