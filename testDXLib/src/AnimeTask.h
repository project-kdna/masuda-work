#ifndef __INCLUDED_ANIMETASK_H__
#define __INCLUDED_ANIMETASK_H__

#include <list>

class AnimeTask
{
public:
	AnimeTask();
	virtual ~AnimeTask();
	
	virtual void initialize() = 0;
	virtual bool run() = 0;
};


class AnimeTaskManager
{
public:
	AnimeTaskManager();
	~AnimeTaskManager();

	void append(AnimeTask* task);
	void release();

	void initialize();
	bool run();

private:
	std::list<AnimeTask*> mp_tasks;
	std::list<AnimeTask*>::iterator m_it;
};


#endif // !__INCLUDED_ANIMETASK_H__
