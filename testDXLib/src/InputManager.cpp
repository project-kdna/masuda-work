#include "InputManager.h"

namespace kdna {


/* static初期化 */
InputManager* InputManager::mp_inst = 0;

/*! @brief コンストラクタ */
ConfigKeyMap::ConfigKeyMap()
{
	mp_configKeyMap = nullptr;
	mp_configMouseMap = nullptr;

	// 初期化
	initialize();
}

/*! @drief デストラクタ */
ConfigKeyMap::~ConfigKeyMap()
{
	if (mp_configKeyMap != nullptr) { delete[] mp_configKeyMap; }
	if (mp_configMouseMap != nullptr) { delete[] mp_configMouseMap; }
}

/*! @brief 初期化 */
void ConfigKeyMap::initialize()
{
	if (mp_configKeyMap == nullptr) {
		mp_configKeyMap = new int[VK::LENGTH];
		for (int i = 0; i < VK::LENGTH; i++)
		{
			mp_configKeyMap[i] = 0;
		}
	}

	/*
	mp_configKeyMap = new int[MOUSE_LENGTH];
	for (int i = 0; i < MOUSE_LENGTH; i++) { mp_configMouseMap[i] = 0; }
	*/
}

/*! @brief キーの割り当て
	@param[in] inputKey 入力キー
	@param[in] configKey 割り当てるキー
	*/
void ConfigKeyMap::assign(int inputKey, int configKey)
{
	assert((inputKey >= 0 || inputKey < KEY_INPUT_MAX) && "inputKey範囲外");
	assert((configKey >= 0 || configKey < VK::LENGTH) && "configKey範囲外");

	mp_configKeyMap[configKey] = inputKey;
}

/*! @brief キーマップをファイルから読み込んで登録
	@param[in] filename ファイル名
	@return 成功ならtrue
	*/
bool ConfigKeyMap::assignFromFile(char* filename) { return false; }

/*! @brief マップを元に入力をゲーム入力に変換する
@param[in] configKey 仮想キー
*/
int ConfigKeyMap::getKeyState(int configKey)
{
	assert((configKey >= 0 || configKey < VK::LENGTH) && "configKey範囲外");
	return mp_configKeyMap[configKey];
}




/*! @brief コンストラクタ */
InputManager::InputManager()
{
	mp_configKeyMap = nullptr;
	mp_keyCounter = nullptr;

	// 初期化
	initialize();
}

/*! @brief デストラクタ */
InputManager::~InputManager()
{
	if (mp_configKeyMap != nullptr) { delete mp_configKeyMap; }
	if (mp_keyCounter != nullptr) { delete[] mp_keyCounter; }
}

/*! @brief 初期化 */
void InputManager::initialize()
{
	if (mp_configKeyMap == nullptr) {
		mp_configKeyMap = new ConfigKeyMap();
	}
	if (mp_keyCounter == nullptr) {
		mp_keyCounter = new int[KEY_INPUT_MAX];
	}
}

/*! @brief キーの割り当て
	@param[in] inputKey 入力キー
	@param[in] configKey 割り当てるキー
	*/
void InputManager::assign(int inputKey, int configKey)
{
	if (mp_configKeyMap != nullptr) {
		// そのままConfigKeyMapに投げる
		return mp_configKeyMap->assign(inputKey, configKey);
	}
}

/*! @brief ファイルからキーを割り当てる
	@param[in] filename 読み込むファイル名
	@return 成功ならtrue
	*/
bool InputManager::assignFromFile(char* filename) { return false; }

/*! @brief キーの生の状態を取得
	@param[in] inputKey 入力キー
	*/
int InputManager::getKeyState(int inputKey)
{
	assert((inputKey >= 0 || inputKey < KEY_INPUT_MAX) && "inputKey範囲外");

	return mp_keyCounter[inputKey];
}

/*! @brief 割り当てたキーの状態を取得
	@param[in] configKey 割り当てたキー
	@param[in] isTrigger trueなら押した瞬間だけ1を返す
	@return キー入力フレーム数(0: 入力なし)
	*/
int InputManager::getConfigKeyState(int configKey, bool isTrigger)
{
	if (mp_configKeyMap) {
		int idx = mp_configKeyMap->getKeyState(configKey);

		if (isTrigger == false) {
			// 押した瞬間だけ0以外を返す
			return (mp_keyCounter[idx] == 1);
		}
		// 押したフレーム数を返す
		return mp_keyCounter[idx];
	}
	return -1;
}

/*! @brief 状態を更新する(キーボード) */
void InputManager::updateKeyboard()
{
	// 入力更新
	char keyBuffer[256];
	if (GetHitKeyStateAll(keyBuffer) != 0) {
		assert("GetHitKeyStateAllエラー");
		return;
	}

	for (int i = 0; i < KEY_INPUT_MAX; i++)
	{
		if (keyBuffer[i]) {
			mp_keyCounter[i]++;
		}
		else {
			mp_keyCounter[i] = 0;
		}
	}
}

/*! @brief 状態を更新する(マウス) */
void InputManager::updateMouse()
{
	if (!GetMousePoint(&m_mx, &m_my) || !(m_mInput = GetMouseInput())) {
		assert("GetMousePointエラー");
		return;
	}
}

/*! @brief 状態を更新する(ゲームパッド) */
void InputManager::updateGamepad() {}

/*! @brief 状態を更新する(すべて) */
void InputManager::updateAll()
{
	updateGamepad();
	updateMouse();
	// TODO: ゲームパッドが有効か調べてから
	//updateGamepad();
}


}