#include "Anime.h"
#include "AnimeController.h"
#include "AnimeTask.h"
#include "DrawManager.h"


/*! @brief コンストラクタ */
FadeAnime::FadeAnime(Image* img, double speed, double alpha)
{
	mp_img = img;
	m_layerIdx = -1;
	initialize();
	mp_actrl->set(alpha, speed);
}

/*! @brief コンストラクタ */
FadeAnime::FadeAnime(int layerIdx, double speed, double alpha)
{
	mp_img = NULL;
	m_layerIdx = layerIdx;
	initialize();
	mp_actrl->set(alpha, speed);
}

/*! @brief デストラクタ */
FadeAnime::~FadeAnime()
{
	delete mp_actrl;
}

/*! @brief 初期化 */
void FadeAnime::initialize()
{
	// アニメーションコントローラの生成
	if (mp_actrl == nullptr) {
		mp_actrl = new AnimeController();
	}
}

/*! @brief フェードアニメーションを実行 */
bool FadeAnime::run()
{
	// 現在の透過度を取得
	double alpha;
	if (mp_img == NULL) {
		alpha = DrawManager::getInstance()->getLayerAlpha(m_layerIdx);
	}
	else {
		alpha = mp_img->getAlpha();
	}
	// アニメーションの開始
	mp_actrl->begin(alpha);

	// 透過度を設定する
	if (mp_img == NULL) {
		DrawManager::getInstance()->setLayerAlpha(m_layerIdx, mp_actrl->getValue());
	}
	else {
		mp_img->setAlpha(mp_actrl->getValue());
	}

	// アニメーションの終了
	if (mp_actrl->end()) { return true; }
	return false;
}


/*! @brief コンストラクタ */
WaitAnime::WaitAnime(double speed)
{
	initialize();
	mp_actrl->set(speed);
}

/*! @brief デストラクタ */
WaitAnime::~WaitAnime()
{
	delete mp_actrl;
}

/*! @brief 初期化 */
void WaitAnime::initialize()
{
	// アニメーションコントローラの生成
	if (mp_actrl == nullptr) {
		mp_actrl = new AnimeController();
	}
}

/*! @brief 実行 */
bool WaitAnime::run()
{
	// アニメーションの開始
	mp_actrl->begin();
	// アニメーションの終了
	if (mp_actrl->end()) { return true; }
	return false;
}