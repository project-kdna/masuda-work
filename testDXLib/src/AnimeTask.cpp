/*! 
* @file AnimeTask.cpp 
* @author Shohei Masuda
*/

#include "AnimeTask.h"

/*! @brief コンストラクタ */
AnimeTask::AnimeTask() {}

/*! @brief デストラクタ */
AnimeTask::~AnimeTask() {}



/*! @brief コンストラクタ */
AnimeTaskManager::AnimeTaskManager()
{
	initialize();
}

/*! @brief デストラクタ */
AnimeTaskManager::~AnimeTaskManager()
{
	this->release();
}

/*! @brief 初期化 */
void AnimeTaskManager::initialize()
{
	// すでに追加されているアニメーションを初期化
	if (mp_tasks.size() != 0) {
		std::list<AnimeTask*>::iterator it;
		for (it = mp_tasks.begin(); it != mp_tasks.end(); ++it)
		{
			(*it)->initialize();
		}
	}
	// イテレータの初期化
	m_it = mp_tasks.begin();
}

/*! @brief アニメーションの追加 
	@param[in] アニメタスクオブジェクト
*/
void AnimeTaskManager::append(AnimeTask* task)
{
	// 追加
	mp_tasks.push_back(task);
	// タスクの初期化
	task->initialize();
	// イテレータの再取得
	m_it = mp_tasks.begin();
}

/*! @brief 登録されているアニメーションを全て削除・開放する 
	@details 登録されたアニメーションは開放されるので注意． 
*/
void AnimeTaskManager::release()
{
	// アニメーションの削除・開放
	if (mp_tasks.size() != 0) {
		std::list<AnimeTask*>::iterator it;
		for (it = mp_tasks.begin(); it != mp_tasks.end(); ++it)
		{
			delete (*it);
		}
		mp_tasks.clear();
	}
}

/*! @brief アニメーションリストを実行する */
bool AnimeTaskManager::run()
{
	if (m_it != mp_tasks.end())
	{
		// 実行
		if ((*m_it)->run() == true) {
			++m_it;
		}
	}
	else {
		return true;		// 全てのアニメーションが実行済み
	}
	return false;			// 実行中
}