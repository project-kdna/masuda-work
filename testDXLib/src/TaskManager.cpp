#include "TaskManager.h"

TaskManager* TaskManager::mp_inst = 0;


/*! @brief コンストラクタ */
TaskManager::TaskManager()
{
	m_isIteratorMoved = false;
}

/*! @brief デストラクタ */
TaskManager::~TaskManager() {}

/*! @brief タスクを追加する
	@details タスクのパラメータを同時に設定できる．
	@param[in] task 追加するタスク
	@param[in] name タスクの識別名
	@param[in] priority 優先度
	@param[in] isAutoDelete タスク終了後に自動的に開放(delete)するか
*/
void TaskManager::append(Task* task, const std::string& name, int priority, bool isAutoDelete)
{
	// タスク基本情報を設定する
	task->setName(name);
	task->setPriority(priority);
	task->setIsAutoDelete(isAutoDelete);
	task->setIsValid(true);

	// タスクリストに追加
	std::list<Task*>::iterator it;
	for (it = mp_tasks.begin(); it != mp_tasks.end(); ++it)
	{
		// 優先度に従う
		if ((*it)->getPriority() > task->getPriority()) {
			// 挿入
			mp_tasks.insert(it, task);
			task->initialize();
			return;
		}
	}

	// タスクリストが空なら，最後に追加
	mp_tasks.push_back(task);
	task->initialize();
}

/*! @brief 指定したタスクを削除する． 
	@param[in] 削除するタスク
*/
/*
void TaskManager::remove(Task* task)
{
	// 引数と一致したタスクを削除する
	std::list<Task*>::iterator it = mp_tasks.begin();
	while (it != mp_tasks.end())
	{
		if ((*it) == task) {
			// 消去するイテレータが現在タスク処理中のものであれば
			// フラグを立てて、タスクのイテレータを次へ移動
			if (it == m_it) {
				m_isIteratorMoved = true;
			}
			if (task->getIsAutoDelete() == true) {
				delete(task);
				task = nullptr;
			}
			m_it = mp_tasks.erase(it);
			break;
		}
		else { ++it; }
	}
}
*/
void TaskManager::remove(Task* task)
{
	// run()呼び出し後に強制的に開放する
	task->setIsValid(false);
	task->setIsAutoDelete(true);
}

/*! @brief タスクを全て解放する．
*/
void TaskManager::clear()
{
	std::list<Task*>::iterator it = mp_tasks.begin();
	while (it != mp_tasks.end())
	{
		this->remove((*it));
		++it;
	}
}

/*! @brief タスクを実行する．
	@return 発生イベント
*/
int TaskManager::run()
{
	int message = 0;				// イベントメッセージ
	std::list<Task*> deleteTasks;	// タスク全実行後に解放するタスク

	// チェック
	if (mp_tasks.empty()) { return 0; }
	// タスクの実行
	
	m_it = mp_tasks.begin();
	while (m_it != mp_tasks.end())
	{
		// 有効なら実行
		if ((*m_it)->getIsValid()) {
			message = (*m_it)->run();
		}

		// この時点で無効かつ解放するタスクを調査
		if (!(*m_it)->getIsValid() && (*m_it)->getIsAutoDelete()) {
			deleteTasks.push_back(*m_it);
		}

		++m_it;

		// イベント発生時は強制終了
		if (message != 0) { break; }

		printf("次\n");
	}

	if (!deleteTasks.empty()) {
		std::list<Task*>::iterator it;
		for (it = deleteTasks.begin(); it != deleteTasks.end(); ++it)
		{
			delete (*it);
			(*it) = nullptr;
		}
	}

	printf("終わり\n");

	return message;
}
