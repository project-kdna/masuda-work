#include "Animation.h"
#include "LinearInterpolator.h"

namespace kdna {

Animation::Animation() {
	mp_drawable = nullptr;
	m_prevTime = 0;
	m_erapsedTime = 0;
	m_hasEnded = true;
}

Animation::~Animation() {}

double Animation::getNormalizedTime() {
	// 経過時間を取得
	double erapsedTime = getErapsedTime();
	// 継続時間が設定されているかチェック
	if (m_duration == 0) { return 0.0; }

	// 0.0 ~ 1.0に正規化した時間を計算
	double t = erapsedTime / m_duration;
	if (t < 0.0) { t = 0.0; }
	else if (t > 1.0) { t = 1.0; }

	return t;
}

int Animation::getErapsedTime() {
	// 時間の差分を計算
	int diff = static_cast<int>(timeGetTime() - m_prevTime);
	if (m_prevTime == 0) { diff = 0; }
	// 経過時間を計算
	m_erapsedTime += diff;

	// 時間の更新
	m_prevTime = timeGetTime();
	return m_erapsedTime;
}

}