#include "Animator.h"
#include "Animation.h"

namespace kdna {

Animator::Animator() {
	m_isAutoDelete = false;
	m_hasEnded = false;
}

Animator::~Animator() {
	clear();
}

void Animator::add(Animation* anime)
{
	mp_animes.push_back(anime);
	m_it = mp_animes.begin();		// イテレータの初期化
}

void Animator::remove(Animation* anime) {
	Itr it = mp_animes.begin();
	while (it != mp_animes.end())
	{
		if ((*it) == anime) {
			if (getIsAutoDelete()) {
				delete (*it);
				(*it) = nullptr;
				printf("消えたよ\n");
			}
			mp_animes.erase(it);
		}
		else {
			++it;
		}
	}
}

void Animator::clear() {

	// TODO ちゃんとやる
	mp_animes.clear();
}

void Animator::start() {
	// チェック
	if (mp_animes.empty()) { return; }

	if (m_it != mp_animes.end()) {
		(*m_it)->start();

		if ((*m_it)->getHasEnded()) {
			++m_it;
			printf("アニメーションおわり\n");
		}
	}
	else {
		m_hasEnded = true;
	}
}

void Animator::reset()
{
	if (m_hasEnded) { return; }

	m_it = mp_animes.begin();
	m_hasEnded = false;
}

}
