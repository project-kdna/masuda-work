#ifndef __INCLUDED_DRAWMANAGER_H__
#define __INCLUDED_DRAWMANAGER_H__

class Drawable;

#include <vector>
#include "Layer.h"

#define MAX_LAYER_NUM 32

namespace kdna {

/*! @brief 描画管理クラス */
class DrawManager
{
private:
	DrawManager();
	DrawManager(const DrawManager& c);				// コピーコンストラクタ禁止(未定義->呼び出したらリンカエラー)
	DrawManager& operator =(const DrawManager& c);	// コピーコンストラクタ禁止(未定義->呼び出したらリンカエラー)

public:
	static DrawManager& getInstance();

	virtual ~DrawManager();

	void add(Drawable* drawable, int layerIdx);
	void replaceLayer(Layer& layer, int layerIdx);
	void remove(Drawable* drawable, int layerIdx);
	void clear(int layerIdx);
	void clearAll();
	void draw();
	double getLayerAlpha(int layerIdx);
	bool getLayerIsVisible(int layerIdx);
	void setLayerAlpha(int layerIdx, double alpha);
	void setLayerIsVisible(int layerIdx, bool isVisible);

private:
	std::vector<Layer*> mp_layers;		//!< レイヤーリスト
	typedef std::vector<Layer*>::iterator Itr;
};

DrawManager& DrawManager::getInstance() {
	static DrawManager mp_inst;
	return mp_inst;
}

inline double DrawManager::getLayerAlpha(int layerIdx) {
	return mp_layers.at(layerIdx)->getAlpha();
}
inline bool DrawManager::getLayerIsVisible(int layerIdx) {
	return mp_layers.at(layerIdx)->getIsVisible();
}
inline void DrawManager::setLayerAlpha(int layerIdx, double alpha) {
	mp_layers.at(layerIdx)->setAlpha(alpha);
}
inline void DrawManager::setLayerIsVisible(int layerIdx, bool isVisible) {
	mp_layers.at(layerIdx)->setIsVisible(isVisible);
}

}
#endif // !__INCLUDED_DRAWMANAGER_H__
