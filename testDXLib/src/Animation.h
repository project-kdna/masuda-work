#ifndef __INCLUDED_ANIMATION_H__
#define __INCLUDED_ANIMATION_H__

#include <Windows.h>
#pragma comment(lib, "winmm.lib")

namespace kdna {

class Drawable;
class Interpolator;

class Animation
{
public:
	Animation();
	virtual ~Animation();

	virtual void start() = 0;
	virtual void reset() = 0;

	void setDuration(int msec);
	void setInterpolator(Interpolator* interpolator);
	void setDrawable(Drawable* drawable);
	bool getHasEnded();

protected:
	void setHasEnded(bool hasEnded);
	double getNormalizedTime();
	int getErapsedTime();
	Interpolator& getInterpolator();
	Drawable& getDrawable();

private:
	Drawable* mp_drawable;
	Interpolator* mp_interpolator;
	int m_duration;
	DWORD m_prevTime;
	int m_erapsedTime;
	bool m_hasEnded;
};


inline void Animation::setHasEnded(bool hasEnded) {
	m_hasEnded = hasEnded;
}
inline void Animation::setDuration(int msec) {
	if (!getHasEnded()) { return; }
	m_duration = msec;
}
inline void Animation::setDrawable(Drawable* drawable) {
	if (!getHasEnded()) { return; }
	mp_drawable = drawable;
}
inline void Animation::setInterpolator(Interpolator* interpolator) {
	mp_interpolator = interpolator;
}
inline Interpolator& Animation::getInterpolator() {
	// 参照返しにより，受け取り側は勝手に開放できない
	return *mp_interpolator;
}
inline Drawable& Animation::getDrawable() {
	// 参照返しにより，受け取り側は勝手に開放できない？
	return *mp_drawable;
}
inline bool Animation::getHasEnded() {
	return m_hasEnded;
}

}
#endif // !__INCLUDED_ANIMATION_H__
