#ifndef __INCLUDED_LAYER_H__
#define __INCLUDED_LAYER_H__

#include "Drawable.h"
#include <vector>

namespace kdna
{

class Layer
	: public Drawable
{
public:
	Layer();
	~Layer();

	void add(Drawable* drawable);
	void remove(Drawable* drawable);
	void replace(std::vector<Drawable*>& drawables);
	Layer* clone();
	void clear();
	void draw();		// override

private:
	std::vector<Drawable*> mp_drawables;
	typedef std::vector<Drawable*>::iterator Itr;
};

}
#endif // !__INCLUDED_LAYER_H__
