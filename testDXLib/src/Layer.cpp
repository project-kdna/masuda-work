#include "Layer.h"
#include "Drawable.h"
#include <DxLib.h>
#include <iterator>		// std::back_inserter
#include <algorithm>	// std::copy

namespace kdna {

/*!
	@brief コンストラクタ
	*/
Layer::Layer() {}

/*!
	@brief デストラクタ
	*/
Layer::~Layer() {}

/*!
	@brief 描画可能オブジェクトを描画リストに追加します．

	このオブジェクトは，描画終了後にリストから除去されます．
	解放はしません．

	@param[in] drawable 描画可能オブジェクト
	*/
void Layer::add(Drawable* drawable)
{
	mp_drawables.push_back(drawable);
}

/*!
	@brief 描画リストを置き換えます．

	オブジェクトが持つ描画リストは除去され，完全に置き換えられます．

	@param[in] drawable 描画リスト
	*/
void Layer::replace(std::vector<Drawable*>& drawables)
{
	// 描画リストを除去
	clear();
	// 置き換え
	std::copy(drawables.begin(), drawables.end(), std::back_inserter(mp_drawables));
}


/*!
	@brief 描画リストから描画可能オブジェクトを解除します．

	@param[in] drawable 解除したい描画可能オブジェクト
	*/
void Layer::remove(Drawable* drawable)
{
	for (Itr it = mp_drawables.begin(); it != mp_drawables.end(); ++it)
	{
		if ((*it) == drawable) {
			it = mp_drawables.erase(it);
		}
	}
}
/*!
	@brief レイヤーのクローンを生成します．

	追加された描画可能オブジェクトのポインタはそのままコピーされます．
	*/
Layer* Layer::clone()
{
	Layer* l = new Layer();
	// プリミティブなメンバをコピー
	l->setPosition(getX(), getY());
	l->setSize(getWidth(), getHeight());
	l->setAlpha(getAlpha());
	l->setColor(getR(), getG(), getB());
	l->setIsVisible(getIsVisible());
	// 描画リストのコピー
	l->replace(mp_drawables);

	return l;
}


/*!
	@brief 描画リストの内容をすべてクリアします．
	*/
void Layer::clear()
{
	mp_drawables.clear();
}

/*!
	@brief レイヤーの描画リストから描画を開始します．

	追加された描画可能オブジェクトは，描画終了後に除去されます．
	*/
void Layer::draw()
{
	double alpha = getAlpha();

	for (Itr it = mp_drawables.begin(); it != mp_drawables.end(); ++it)
	{
		if (getIsVisible() == true)
		{
			// 描画するオブジェクトはレイヤーの透過度に影響される
			double alpha2 = (*it)->getAlpha();
			(*it)->setAlpha(alpha * alpha2);

			// 描画は任せる
			(*it)->draw();

			// 透過度を元に戻す
			(*it)->setAlpha(alpha2);
		}
	}
}

}
