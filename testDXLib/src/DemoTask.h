
#ifndef __INCLUDED_DEMO_H__
#define __INCLUDED_DEMO_H__

#include "Task.h"
#include "TitleTask.h"
#include "AnimeTask.h"
#include "Anime.h"
#include "DrawManager.h"

#include "Provider.h"

class DemoTask :
	public Task
{
public:
	DemoTask();
	~DemoTask();

	void initialize();
	int run();

private:
	GraphProvider m_gp;
	AnimeTaskManager m_amgr;
};

#endif // !__INCLUDED_DEMO_H__

