#ifndef __INCLUDED_ANIME_H__
#define __INCLUDED_ANIME_H__

#include "DrawManager.h"
#include "AnimeTask.h"

class FadeAnime
	: public AnimeTask
{
public:
	FadeAnime(Image* img, double speed, double alpha);
	FadeAnime(int layerIdx, double speed, double alpha);
	~FadeAnime();

	void initialize();
	bool run();

private:
	Image* mp_img;
	
	int m_layerIdx;
	double m_alpha;
	double m_targetAlpha;
	double m_speed;
	double m_unitValue;
};


class WaitAnime
	: public AnimeTask
{
public:
	WaitAnime(double speed);
	~WaitAnime();

	void initialize();
	bool run();

private:
	double m_speed;
};

#endif // !__INCLUDED_ANIME_H__
