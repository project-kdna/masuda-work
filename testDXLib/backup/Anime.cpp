#include "Anime.h"

/*! @brief コンストラクタ */
FadeAnime::FadeAnime(Image* img, double speed, double alpha)
{
	mp_img = img;
	m_layerIdx = -1;
	m_targetAlpha = alpha;
	m_speed = speed;
}

/*! @brief コンストラクタ */
FadeAnime::FadeAnime(int layerIdx, double speed, double alpha)
{
	mp_img = NULL;
	m_layerIdx = layerIdx;
	m_targetAlpha = alpha;
	m_speed = speed;
}

/*! @brief デストラクタ */
FadeAnime::~FadeAnime() {}

/*! @brief 初期化 */
void FadeAnime::initialize()
{
	m_counter = 0;
	m_countMax = static_cast<int>(m_speed * m_unitFps);
}

/*! @brief フェードアニメーションを実行 */
bool FadeAnime::run()
{
	if (m_counter == 0) {
		// 現在の透過度を取得
		if (mp_img == NULL) {
			m_alpha = DrawManager::getInstance()->getLayerAlpha(m_layerIdx);
		}
		else {
			m_alpha = mp_img->getAlpha();
		}

		if (m_countMax != 0) {
			// 単位カウントあたり変化量を設定
			m_unitValue = (m_targetAlpha - m_alpha) / m_countMax;
		} else {
			m_unitValue = (m_targetAlpha - m_alpha);
		}
	}

	// 実行中の場合
	if (m_counter < m_countMax) {

		// 透過度を変化させる
		m_alpha += m_unitValue;
		printf("alpha: %f\n", m_alpha);
		printf("unitValue: %f\n", m_unitValue);

		// 透過度を設定する
		if (mp_img == NULL) {
			DrawManager::getInstance()->setLayerAlpha(m_layerIdx, m_alpha);
		}
		else {
			mp_img->setAlpha(m_alpha);
		}

		// カウンタを回す
		m_counter++;
	}
	else {
		// 透過度を目標値に設定する
		if (mp_img == NULL) {
			DrawManager::getInstance()->setLayerAlpha(m_layerIdx, m_targetAlpha);
		}
		else {
			mp_img->setAlpha(m_targetAlpha);
		}

		return true;
	}
	return false;
}


/*! @brief コンストラクタ */
WaitAnime::WaitAnime(double speed)
{
	m_speed = speed;
}

/*! @brief デストラクタ */
WaitAnime::~WaitAnime() {}

/*! @brief 初期化 */
void WaitAnime::initialize()
{
	m_counter = 0;
	m_countMax = static_cast<int>(m_speed * m_unitFps);
}

/*! @brief 実行 */
bool WaitAnime::run()
{
	// 実行中の場合
	if (m_counter < m_countMax) {
		m_counter++;
	}
	// 終了
	else {
		return true;
	}
	return false;
}