#include "TextImage.h"

/*! @brief コンストラクタ */
TextImage::TextImage()
{
	setPosition(0, 0);
	setColor(255, 255, 255);
	m_width = 0;
	m_lineSpace = 0;
	m_strings.reserve(10);
	setDefaultFont();
}

TextImage::TextImage(char* str)
{
	setString(str);
}

TextImage::TextImage(char* str, int fontHandle, int r, int g, int b)
{
	setFont(fontHandle);
	setColor(r, g, b);
	setString(str);
}

/*! @brief デストラクタ */
TextImage::~TextImage() {}

/*! @brief デフォルトフォントに設定する */
void TextImage::setDefaultFont()
{
	// デフォルトフォント("", 16, 6)
	setFont(GetDefaultFontHandle());
}


/*! @brief 表示するフォントを設定する
	@param[in] fontname フォント名 
	@param[in] size フォントサイズ
*/
void TextImage::setFont(char* fontname, int size, int thick)
{
	// フォントの登録
	this->setFont(CreateFontToHandle(fontname, size, thick, DX_FONTTYPE_NORMAL));
}

/*! @brief 表示するフォントを設定する 
	@param[in] fontHandle フォントハンドル
*/
void TextImage::setFont(int fontHandle)
{
	// 保持フォントを削除
	if (m_fontHandle != GetDefaultFontHandle()) {
		DeleteFontToHandle(m_fontHandle);
	}
	// フォントの設定
	m_fontHandle = fontHandle;
	// フォントサイズを取得
	m_textSize = GetFontSizeToHandle(fontHandle);
}


/*! @brief 描画する文字列を設定する 
	@param[in] str 文字列
*/
void TextImage::setString(char* str)
{
	// 文字列をエスケープに合わせて分割する．
	m_strings = this->split(str, "\n");
	// 文字列の最大横幅を計算する
	int maxWidth = 0;
	int lines = -1;
	std::vector<std::string>::iterator it;
	for (it = m_strings.begin(); it != m_strings.end(); ++it)
	{
		int w = GetDrawStringWidthToHandle(it->c_str(), it->length(), m_fontHandle);
		if (maxWidth < w) { maxWidth = w; }
		lines++;
	}
	m_width = maxWidth;
	// 文字列の全体高さを取得する．
	m_height = m_textSize + lines * (m_lineSpace + m_textSize);
}

/*! @brief 描画する文字列を設定する 
	@param[in] fmt フォーマット文字列
*/
void TextImage::setFormatString(char* fmt, ...)
{
	char buf[1024];
	va_list ap;
	va_start(ap, fmt);
	vsprintf_s(buf, 1024, fmt, ap);
	va_end(ap);

	setString(buf);
}


void TextImage::draw()
{
	if (m_alpha > 0.0) {
		if (m_alpha < 255.0) { SetDrawBlendMode(DX_BLENDMODE_ALPHA, (int)m_alpha); }

		//DrawBox(m_x, m_y, m_x + m_width, m_y + m_height, GetColor(255, 0, 0), false);

		// 上手く改行して表示する
		int lines = 0;
		std::vector<std::string>::iterator it;
		for (it = m_strings.begin(); it != m_strings.end(); ++it)
		{
			DrawStringToHandle(m_x, m_y + lines * (m_textSize + m_lineSpace), it->c_str(), GetColor(m_r, m_g, m_b), m_fontHandle);
			lines++;
		}
		
		if (m_alpha < 255.0) { SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0); }
	}
}