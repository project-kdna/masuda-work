#include "DiagramImage.h"

/*! @brief コンストラクタ */
RectangleImage::RectangleImage()
{
	m_r = m_g = m_b = 0;
	m_isFill = true;
}

/*! @brief コンストラクタ */
RectangleImage::RectangleImage(int x, int y, int width, int height, int r, int g, int b, double alpha, bool isFill)
{
	setPosition(x, y);
	setSize(width, height);
	setAlpha(alpha);
	m_r = r;
	m_g = g;
	m_b = b;
	m_isFill = isFill;
}

/*! @brief デストラクタ */
RectangleImage::~RectangleImage() {}

/*! @brief 矩形を描画 */
void RectangleImage::draw()
{
	// 矩形を描画する
	if (m_alpha > 0.0) {
		if (m_alpha < 255.0) { SetDrawBlendMode(DX_BLENDMODE_ALPHA, (int)m_alpha); }

		DrawBox(m_x, m_y, m_x + m_width, m_y + m_height, GetColor(m_r, m_g, m_b), m_isFill);

		if (m_alpha < 255.0) { SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0); }
	}
}