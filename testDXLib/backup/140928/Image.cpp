#include "Image.h"

/*! @brief コンストラクタ */
Image::Image() :
	m_x(0),
	m_y(0),
	m_width(0),
	m_height(0),
	m_alpha(0.0)
{}

/*! @brief コンストラクタ */
Image::Image(int x, int y, int width, int height, int alpha) :
	m_x(x),
	m_y(y),
	m_width(width),
	m_height(height),
	m_alpha(alpha)
{}


/*! @brief デストラクタ */
Image::~Image() {}