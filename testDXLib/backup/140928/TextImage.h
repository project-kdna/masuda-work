#ifndef __INCLUDED_TEXTIMAGE_H__
#define __INCLUDED_TEXTIMAGE_H__

#include "Image.h"
#include <DxLib.h>
#include <sstream>
#include <vector>
#include <cstdarg>

class TextImage
	: public Image
{
public:
	TextImage();
	TextImage(char* str);
	TextImage(char* str, int fontHandle, int r, int g, int b);
	~TextImage();

	// Override
	void draw();

	void setFont(char* fontname, int size, int thick);
	void setFont(int fontHandle);
	void setDefaultFont();
	void setString(char* str);
	void setFormatString(char* fmt, ... );
	
	void setColor(int r, int g, int b) { m_r = r; m_g = g; m_b = b; }
	void setLineSpace(int lineSpace) { m_lineSpace = lineSpace; }
	// Override
	void setSize(int width, int height) { /* サイズは外部から変更できない */ }

private:
	std::vector<std::string> split(const std::string& str, const std::string& delim)
	{
		std::vector<std::string> res;
		int cur = 0;
		int found;
		int delimLength = delim.size();
		while ((found = str.find(delim, cur)) != std::string::npos)
		{
			res.push_back(std::string(str, cur, found - cur));
			cur = found + delimLength;
		}
		res.push_back(std::string(str, cur, str.size() - cur));
		return res;
	}

private:
	std::vector<std::string> m_strings;	//!< 描画する文字列
	int m_textSize;						//!< 文字の大きさ
	int m_lineSpace;					//<! 行間
	int m_textWidth;					//!< 描画しているテキストの横幅
	int m_r, m_g, m_b;					//!< テキストの色
	int m_fontHandle;					//!< フォントハンドル
};

#endif // !__INCLUDED_TEXTIMAGE_H__
