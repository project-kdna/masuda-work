#ifndef __INCLUDED_DIAGRAMIMAGE_H__
#define __INCLUDED_DIAGRAMIMAGE_H__

#include "Image.h"
#include <DxLib.h>

/*! @brief 矩形クラス */
class RectangleImage
	: public Image
{
public:
	RectangleImage();
	RectangleImage(int x, int y, int width, int height, int r, int g, int b, double alpha = 255.0, bool isFill = true);
	~RectangleImage();

	void draw();

private:
	int m_r, m_g, m_b;			//!< 矩形の色
	bool m_isFill;				//!< 矩形を塗りつぶすかどうか
};

#endif // !__INCLUDED_DIAGRAMIMAGE_H__
