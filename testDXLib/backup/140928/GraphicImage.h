#ifndef __INCLUDED_GRAPHICIMAGE_H__
#define __INCLUDED_GRAPHICIMAGE_H__

#include "Image.h"
#include <DxLib.h>

/*! @brief 画像クラス */
class GraphicImage
	:public Image
{
public:
	GraphicImage();
	GraphicImage(int handle, double alpha = 255.0, int x = 0, int y = 0);
	virtual ~GraphicImage();

	void draw();

private:
	int m_handle;				//!< 画像ハンドル
};

#endif // !__INCLUDED_GRAPHICIMAGE_H__
