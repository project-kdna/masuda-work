#include "GraphicImage.h"

/*! @brief コンストラクタ */
GraphicImage::GraphicImage()
{
	m_handle = -1;
}

/*! @brief コンストラクタ */
GraphicImage::GraphicImage(int handle, double alpha, int x, int y)
{
	int w, h;
	GetGraphSize(m_handle, &w, &h);

	m_handle = handle;
	setPosition(x, y);
	setSize(w, h);
	setAlpha(alpha);
}

/*! @brief デストラクタ */
GraphicImage::~GraphicImage() {}

/*! @brief 画像を描画 */
void GraphicImage::draw()
{
	// 画像を描画する
	if (m_alpha > 0.0) {
		if (m_alpha < 255.0) { SetDrawBlendMode(DX_BLENDMODE_ALPHA, (int)m_alpha); }

		DrawGraph(m_x, m_y, m_handle, true);

		if (m_alpha < 255.0) { SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0); }
	}
}