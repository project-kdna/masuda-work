#ifndef __INCLUDED_IMAGE_H__
#define __INCLUDED_IMAGE_H__

/*! @brief イメージ親クラス */
class Image
{
public:
	Image();
	Image(int x, int y, int width, int height, int alpha);
	virtual ~Image();

	virtual void draw()	= 0;

	void setPosition(int x, int y) { m_x = x; m_y = y; }
	void setSize(int width, int height) { m_width = width; m_height = height; }
	void setAlpha(double alpha) { m_alpha = alpha; }

	void getPosition(int& x, int& y) { x = m_x; y = m_y; }
	void getSize(int& width, int& height) { width = m_width; height = m_height; }
	double getAlpha() { return m_alpha; }

protected:
	int m_x, m_y;			//!< 座標		
	int m_width, m_height;	//!< サイズ
	double m_alpha;			//!< 透過度
};

#endif // !__INCLUDED_IMAGE_H__
