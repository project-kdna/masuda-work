#ifndef __INCLUDED_TITLETASK_H__
#define __INCLUDED_TITLETASK_H__

#include "InputManager.h"
#include "DrawManager.h"
#include "Task.h"
#include "AnimeTask.h"
#include "Anime.h"
#include "MenuBox.h"

#include "Image.h"
#include "Text.h"

#include "ResourceProvider.h"
#include "Provider.h"

#include "SaveLoadTask.h"

class TitleTask
	: public Task
{
public:
	TitleTask();
	~TitleTask();

	void initialize();
	int run();

private:
	void selectItem();

private:
	enum class Item : int {
		NewGame,
		LoadGame,
		Quit,
	};
	enum class State : int {
		Start,
		Select,
		Selected,
		End,
	};

	State m_taskState;			//<! 現在の状態
	GraphProvider m_gp;			//<! 画像提供クラス
	TextProvider m_tp;			//<! 文字提供クラス
	MenuBox m_mbox;
	AnimeTaskManager m_amgr;			//<! アニメーション管理
	Task* mp_next;						//<! 次タスク
	int m_idx;							//<! カーソルインデックス
};

#endif // !__INCLUDED_TITLETASK_H__
