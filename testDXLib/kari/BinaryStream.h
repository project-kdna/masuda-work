#ifndef __INCLUDED_BINARYSTREAM_H__
#define __INCLUDED_BINARYSTREAM_H__

class BinaryStream
{
public:

	BinaryStream(unsigned char* binary, int bitCount = 1);


private:
	unsigned char* mp_binary;
	int m_bitCount;
	
	
	int m_width;
	int m_height;
	int m_chipWidth;
	int m_chipHeight;
};

#endif // !__INCLUDED_BINARYSTREAM_H__
