#include "ImageCreator.h"

ImageCreator::ImageCreator()
{
	m_fontHandle = -1;
	m_color = 0x000000;
	m_lineSpace = 0;
	m_alpha = 1.0;
}


Image* ImageCreator::createGraphic(int handle)
{
	return this->createGraphic(handle, 0, 0, m_alpha);
}

Image* ImageCreator::createGraphic(int handle, int x, int y)
{
	return this->createGraphic(handle, x, y, m_alpha);
}

Image* ImageCreator::createGraphic(int handle, int x, int y, double alpha)
{
	Image* img = new Image(handle);
	img->setPosition(x, y);
	img->setAlpha(alpha * 255.0);

	return img;
}

Text* ImageCreator::createText(int x, int y, const char* str)
{
	return this->createText(x, y, str, m_color, m_fontHandle, m_lineSpace);
}

Text* ImageCreator::createText(int x, int y, const char* str, int color)
{
	return this->createText(x, y, str, color, m_fontHandle, m_lineSpace);
}

Text* ImageCreator::createText(int x, int y, const char* str, int color, int fontHandle)
{
	return this->createText(x, y, str, color, fontHandle, m_lineSpace);
}

Text* ImageCreator::createText(int x, int y, const char* str, int color, int fontHandle, int lineSpace)
{
	Text* img = new Text(x, y, str);
	img->setTextProperty(fontHandle, color, lineSpace);
	
	return img;
}

Rectangle* ImageCreator::createRectangle(int x, int y, int width, int height, int color, bool isFill)
{
	return createRectangle(x, y, width, height, color, 1.0, isFill);
}

Rectangle* ImageCreator::createRectangle(int x, int y, int width, int height, int color, double alpha, bool isFill)
{
	Rectangle* img = new Rectangle(x, y, width, height, color, isFill);
	img->setAlpha(alpha);

	return img;
}