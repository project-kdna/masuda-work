#ifndef __INCLUDED_IMAGECREATOR_H__
#define __INCLUDED_IMAGECREATOR_H__

#include "Image.h"
#include "Text.h"
#include "DiagramImage.h"
#include "Image.h"

class ImageCreator
{
public:
	ImageCreator();

	Image* createGraphic(int handle);
	Image* createGraphic(int handle, int x, int y);
	Image* createGraphic(int handle, int x, int y, double alpha);

	Text* createText(int x, int y, const char* str);
	Text* createText(int x, int y, const char* str, int color);
	Text* createText(int x, int y, const char* str, int color, int fontHandle);
	Text* createText(int x, int y, const char* str, int color, int fontHandle, int lineSpace);

	Rectangle* createRectangle(int x, int y, int width, int height, int color, bool isFill = true);
	Rectangle* createRectangle(int x, int y, int width, int height, int color, double alpha, bool isFill = true);

	void setDefaultProperty(int color = 0x000000, double alpha = 1.0, int fontHandle = -1, int linespace = 0)
	{
		// �`�F�b�N
		if (color < 0) { color = 0x000000; }
		if (color > 0xFFFFFF) { color = 0xFFFFFF; }
		if (alpha < 0.0) { alpha = 0.0; }
		if (alpha > 1.0) { alpha = 1.0; }

		m_alpha = alpha;
		m_fontHandle = fontHandle;
		m_lineSpace = linespace;
		m_color = color;
		this->getRGB(color, m_r, m_g, m_b);
	}

private:
	void getRGB(int hexColor, int& r, int& g, int& b)
	{
		r = hexColor >> 16 && 0xFF;
		g = hexColor >> 8  && 0xFF;
		b = hexColor       && 0xFF;
	}

private:
	int m_fontHandle;
	int m_r, m_g, m_b;
	int m_color;
	int m_lineSpace;
	double m_alpha;

};

#endif // !__INCLUDED_IMAGECREATOR_H__
