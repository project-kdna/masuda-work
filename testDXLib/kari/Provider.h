#ifndef __INCLUDED_PROVIDER_H__
#define __INCLUDED_PROVIDER_H__

#include "ResourceProvider.h"
#include "Image.h"
#include "TextImage.h"

class GraphProvider
	: public ResourceProvider<Image>
{
public:
	GraphProvider();
	~GraphProvider();

	void create(std::string key, int handle);
	void create(std::string key, char* filename);
	void setInitAlpha(double alpha) { m_alpha = alpha; }

private:
	double m_alpha;
};

class TextProvider
	: public ResourceProvider<Text>
{
public:
	TextProvider();
	~TextProvider();

	void create(std::string key, char* str);
	
	void setInitFont(int handle);
	void setInitColor(int r, int g, int b) { m_r = r; m_g = g; m_b = b; }
	void setInitAlpha(double alpha) { m_alpha = alpha; }

private:
	int m_fontHandle;			//<! 初期フォントハンドル
	int m_r, m_g, m_b;			//<! 初期文字色
	double m_alpha;				//<! 初期透過度
};

#endif // !__INCLUDED_PROVIDER_H__
