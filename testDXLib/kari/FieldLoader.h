#ifndef __INCLUDED_FIELDLOADER_H__
#define __INCLUDED_FIELDLOADER_H__

#include <fstream>
#include <iostream>
#include <Windows.h>



class FieldLoader
{
public:
	~FieldLoader();

	bool loadMapFile(const char* filename);

	bool isOpen() { return (mp_layer != nullptr); }
	unsigned char* getLayerAddress(int layerIdx) const;
	void getSize(int& w, int& h) const { w = (int)m_fmf.dwWidth; h = (int)m_fmf.dwHeight; }
	void getChipSize(int& w, int& h) const { w = (int)m_fmf.byChipWidth; h = (int)m_fmf.byChipHeight; }
	int getLayerCount() const { return (int)m_fmf.byLayerCount; }

private:
	struct FMFHeader {
		DWORD	dwIdentifier;	// ファイル識別子 'FMF_'
		DWORD	dwSize;			// ヘッダを除いたデータサイズ
		DWORD	dwWidth;		// マップの横幅
		DWORD	dwHeight;		// マップの高さ
		BYTE	byChipWidth;	// マップチップ1つの幅(pixel)
		BYTE	byChipHeight;	// マップチップ1つの高さ(pixel)
		BYTE	byLayerCount;	// レイヤーの数
		BYTE	byBitCount;		// レイヤデータのビットカウント
	};
	FMFHeader m_fmf;

	unsigned char* mp_layer;

	int getFileSize(const char* filename);
	DWORD getDWORD(const unsigned char* p) {
		return (*(p + 3) << 24 | *(p + 2) << 16 | *(p + 1) << 8 | (*p));
	}
	BYTE getBYTE(const unsigned char* p) {
		return (*p);
	}
};

#endif // !__INCLUDED_FIELDLOADER_H__
