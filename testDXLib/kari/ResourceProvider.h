#ifndef __INCLUDED_RESOURCEPROVIDER_H__
#define __INCLUDED_RESOURCEPROVIDER_H__

#include <unordered_map>
#include <string>

template <typename T>
class ResourceProvider
{
public:
	ResourceProvider() {}
	virtual ~ResourceProvider() { this->clear(); }

	/*! @brief キーを削除する 
		@details キーに対応する値は自動的に開放される． 
		@param[in] key キー
	*/
	void remove(std::string key)
	{
		delete m_hash.at(key);
		m_hash.erase(key);
	}

	/*! @brief リソースリストが空かどうか返す
		@return trueなら空
	*/
	bool isEmpty()
	{
		return m_hash.empty()
	}

	/*! @brief 値が空かどうか返す
		@param[in] key キー
		@return trueなら空 
	*/
	bool isEmpty(std::string key)
	{
		if (m_hash.count(key) == 0) {
			return true;
		}
		return false;
	}

	/*! @brief キーを全て削除する 
		@details 全ての値は自動的に開放される．
	*/
	void clear()
	{
		std::unordered_map<std::string, T*>::iterator it = m_hash.begin();
		while (it != m_hash.end())
		{
			std::pair<std::string, T*> p = (*it);
			delete p.second;
			it = m_hash.erase(it);
		}
	}

	/*! @brief キーに対応した値を返す
		@details キーが既に存在する場合，値を変更する．
		@param[in] key キー
	*/
	T*& at(std::string key)
	{
		std::unordered_map<std::string, T*>::iterator it = m_hash.find(key);
		if (it != m_hash.end()) {
			// キーが存在する
			return (*it).second;
		}
		// キーが存在しない
		return m_hash[key];		// 代入元の参照を返す
	}

	/*! @brief キーに対応した値を返す
		@details キーが既に存在する場合，値を変更する．
		@param[in] key キー
	*/
	T*& operator[](std::string key)
	{
		return m_hash[key];		// 代入元の参照を返す
	}

private:
	std::unordered_map<std::string, T*> m_hash;
};

#endif // !__INCLUDED_RESOURCEPROVIDER_H__
