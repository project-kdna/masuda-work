#include "FieldLoader.h"


FieldLoader::~FieldLoader()
{
	delete[] mp_layer;
}


bool FieldLoader::loadMapFile(const char* filename)
{
	// ファイルの読み込み
	FILE *fp;
	if ((fopen_s(&fp, filename, "rb")) != 0) { return false; }
	// ファイルヘッダを読み込む
	unsigned char h[20];
	if (fread(h, sizeof(h), 1, fp) != 1) {
		printf("error: ファイルメモリ読み込み失敗\n");
		return false;
	}
	
	m_fmf.dwIdentifier =	getDWORD(&h[0]);
	m_fmf.dwSize =			getDWORD(&h[4]);
	m_fmf.dwWidth =			getDWORD(&h[8]);
	m_fmf.dwHeight =		getDWORD(&h[12]);
	m_fmf.byChipWidth =		(BYTE)(h[16]);
	m_fmf.byChipHeight =	(BYTE)(h[17]);
	m_fmf.byLayerCount =	(BYTE)(h[18]);
	m_fmf.byBitCount =		(BYTE)(h[19]);

	printf("%x\n", m_fmf.dwIdentifier);
	printf("%x\n", m_fmf.dwSize);
	printf("%x\n", m_fmf.dwWidth);
	printf("%x\n", m_fmf.dwHeight);
	printf("%x\n", m_fmf.byChipWidth);
	printf("%x\n", m_fmf.byChipHeight);
	printf("%x\n", m_fmf.byLayerCount);

	// 識別子の確認
	if (memcmp(&m_fmf.dwIdentifier, "FMF_", 4) != 0) {
		printf("error: ファイルの種類が違う\n");
		return false;
	}
	// メモリの確保
	mp_layer = new unsigned char[m_fmf.dwSize];
	// レイヤーデータの読み込み
	if (fread(mp_layer, sizeof(mp_layer), 1, fp) != 1) {
		printf("error: レイヤーメモリ読み込み失敗\n");
		return false;
	}
	
	// 開放
	fclose(fp);

	return true;
}


unsigned char* FieldLoader::getLayerAddress(int layerIdx) const
{
	// メモリチェック
	if (mp_layer == nullptr || layerIdx >= (int)m_fmf.byLayerCount) {
		return nullptr;
	}

	int bySize = ((int)m_fmf.byBitCount / 8);
	int size = (int)m_fmf.dwWidth * (int)m_fmf.dwHeight * bySize;
	// 指定レイヤデータのアドレスは計算で求められる
	return mp_layer + size;
}

int FieldLoader::getFileSize(const char* filename)
{
	fpos_t filesize = 0;		// ファイルサイズ
	// ファイルを開く
	FILE *fp;
	if ((fopen_s(&fp, filename, "rb")) != 0) { return -1; }
	// ファイルサイズの調査
	fseek(fp, 0, SEEK_END);
	fgetpos(fp, &filesize);
	// ファイルを閉じる
	fclose(fp);

	return static_cast<int>(filesize);
}