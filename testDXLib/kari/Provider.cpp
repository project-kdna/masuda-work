#include "Provider.h"

GraphProvider::GraphProvider()
{
	m_alpha = 0.0;
}

GraphProvider::~GraphProvider() {}

void GraphProvider::create(std::string key, int handle)
{
	if (this->isEmpty(key)) {
		(*this)[key] = new GraphicImage(handle);
		(*this)[key]->setAlpha(m_alpha);
	}
}

void GraphProvider::create(std::string key, char* filename)
{
	create(key, LoadGraph(filename));
}



TextProvider::TextProvider()
{
	m_fontHandle = GetDefaultFontHandle();
	m_r = m_g = m_b = 255;
}

TextProvider::~TextProvider()
{
	// 登録してあるフォントハンドルの開放
	if (m_fontHandle != GetDefaultFontHandle()) {
		DeleteFontToHandle(m_fontHandle);
	}
}

void TextProvider::create(std::string key, char* str)
{
	if (this->isEmpty(key)) {
		(*this)[key] = new TextImage(str, m_fontHandle, m_r, m_g, m_b);
		(*this)[key]->setAlpha(m_alpha);
	}
}

void TextProvider::setInitFont(int handle)
{
	// 登録してあるフォントハンドルの開放
	if (m_fontHandle != GetDefaultFontHandle()) {
		DeleteFontToHandle(m_fontHandle);
	}
	// フォントの登録
	m_fontHandle = handle;
}