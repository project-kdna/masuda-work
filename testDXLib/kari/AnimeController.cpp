#include "AnimeController.h"

/*! @brief コンストラクタ */
AnimeController::AnimeController()
{
	m_targetValue	= 0;
	m_speed			= 0;
	m_value			= 0;
	m_unitValue		= 0;
	m_counter		= 0;
	m_countMax		= 0;
	setFrameRate(60.0);
}

/*! @brief デストラクタ */
AnimeController::~AnimeController() {}

/*! @brief アニメーション開始 */
bool AnimeController::begin(double initValue)
{
	if (m_counter == 0) {
		// 値の設定
		m_value = initValue;
		// 目標カウンタ値の設定
		m_countMax = static_cast<int>(m_speed * m_unitFps);
		// 単位カウントあたり変化量を設定
		if (m_countMax != 0) {
			m_unitValue = (m_targetValue - m_value) / m_countMax;
		}
		else {
			m_unitValue = (m_targetValue - m_value);
		}
		return true;
	}
	return false;
}

/*! @brief アニメーション終了 */
bool AnimeController::end()
{
	if (m_counter >= m_countMax) {
		// 変更値を目標値に設定する
		m_value = m_targetValue;
		return true;
	}
	// 変化量に基づき変化させる
	m_value += m_unitValue;
	// カウンタを回す
	m_counter++;

	return false;
}