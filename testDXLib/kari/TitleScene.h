#ifndef __INCLUDED_TITLESCENE_H__
#define __INCLUDED_TITLESCENE_H__

#include "InputManager.h"
#include "DrawManager.h"
#include "Image.h"
#include "Scene.h"
#include "Task.h"
#include "DemoTask.h"
#include "TitleTask.h"
#include "common.h"

class TitleScene :
	public Scene
{
public:
	TitleScene(Scene* parent);
	virtual ~TitleScene();

	void initialize();
	Scene* run();
	void draw();

private:
	Image* mp_chara;
};


#endif // !__INCLUDED_TITLESCENE_H__
