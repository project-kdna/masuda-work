#ifndef __INCLUDED_CHARACTOR_H__
#define __INCLUDED_CHARACTOR_H__

#include "Task.h"
#include "InputManager.h"
#include "DrawManager.h"
#include "common.h"

class Charactor
	: public Task
{
public:
	Charactor(TaskManager* manager) :
		Task(manager)
	{
		mp_img = NULL;
	}

	Charactor(TaskManager* manager, Image* img) :
		Task(manager)
	{
		mp_img = img;
		m_x = m_y = 0;
	}

	void setImage(Image* img) { mp_img = img; }

protected:
	Image* mp_img;		//!< キャラクター画像
	int m_x, m_y;		//!< 座標
};


class Player :
	public Charactor
{
public:
	Player(TaskManager* manager) :
		Charactor(manager)
	{
		InputManager* idev = InputManager::getInstance();
		idev->assign(KEY_INPUT_LEFT, KEY_LEFT);
		idev->assign(KEY_INPUT_RIGHT, KEY_RIGHT);
	}

	bool run()
	{
		// 初期化処理
		if (m_isProcessed)
		{
			m_isProcessed = false;
			m_x = 320;
			m_y = 240;
		}

		// 入力
		int dx = 0, dy = 0;
		InputManager* idev = InputManager::getInstance();
		if (idev->getConfigKeyState(KEY_LEFT)) {
			dx = -2;
		}
		if (idev->getConfigKeyState(KEY_RIGHT)) {
			dx = 2;
		}

		m_x += dx;
		m_y += dy;
		mp_img->setPosition(m_x, m_y);


		// 描画
		DrawManager* dMng = DrawManager::getInstance();
		dMng->append(mp_img, 2);


		
		m_isProcessed = true;
		return m_isProcessed;
	}
};




#endif // !__INCLUDED_CHARACTER_H__
