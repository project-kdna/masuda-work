#ifndef __INCLUDED_ANIMECONTROLLER_H__
#define __INCLUDED_ANIMECONTROLLER_H__

class AnimeController
{
public:
	AnimeController();
	~AnimeController();

	bool begin(double initValue = 0);
	bool end();

	int getCount() { return m_counter; }
	double getValue() { return m_value; }

	void set(double target, double speed)
	{
		m_targetValue = target;
		m_speed = speed;
	}
	void set(double speed)
	{
		m_speed = speed;
	}
	void setFrameRate(double fps) { m_unitFps = fps / 1000.0; }

private:
	int m_counter;				//!< カウンタ
	int m_countMax;				//!< 最大カウンタ値
	double m_unitFps;			//!< カウンタ単位のFPS
	double m_speed;				//!< アニメーションスピード
	double m_initValue;			//!< 初期値
	double m_value;				//!< 現在の値
	double m_unitValue;			//!< カウンタ単位の値
	double m_targetValue;		//!< 目標値
};

#endif // !__INCLUDED_ANIMECONTROLLER_H__
