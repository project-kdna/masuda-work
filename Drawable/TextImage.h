#ifndef __INCLUDED_TEXTIMAGE_H__
#define __INCLUDED_TEXTIMAGE_H__

#include "Drawable.h"
#include <string>
#include <vector>

class TextImage
	: public Drawable
{
public:
	TextImage();
	TextImage(int x, int y, const char* str);
	~TextImage();

	void setText(const char* str);
	void setText(int x, int y, const char* str);
	void setFormatText(const char* fmt, ... );
	void setFontHandle(int handle);
	void setTextSize(int size);
	void setTextColor(int color);
	void setLineSpace(int space);
	
	// Override
	void draw();

private:
	std::vector<std::string> split(const std::string& str, const std::string& delim);

private:
	std::vector<std::string> m_strings;	//!< 描画する文字列
	int m_fontHandle;					//!< フォントハンドル
	int m_textSize;						//!< 文字の大きさ
	int m_lineSpace;					//<! 行間
	int m_color;						//!< テキストの色
};

inline void TextImage::setTextSize(int size) { m_textSize = size; }
inline void TextImage::setTextColor(int color) { m_color = color; }
inline void TextImage::setLineSpace(int space) { m_lineSpace = space; }

inline std::vector<std::string> TextImage::split(const std::string& str, const std::string& delim)
{
	std::vector<std::string> res;
	int cur = 0;
	int found;
	int delimLength = delim.size();
	while ((found = str.find(delim, cur)) != std::string::npos)
	{
		res.push_back(std::string(str, cur, found - cur));
		cur = found + delimLength;
	}
	res.push_back(std::string(str, cur, str.size() - cur));
	return res;
}

#endif // !__INCLUDED_TEXTIMAGE_H__
