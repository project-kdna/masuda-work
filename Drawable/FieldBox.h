/*! @file FieldBox.h 
	@brief マップのフィールドを一つにまとめたもの 
	@author Shohei Masuda 
*/

#ifndef __INCLUDED_FIELDBOX_H__
#define __INCLUDED_FIELDBOX_H__

#include "Image.h"
#include "FieldReader.h"
#include <DxLib.h>
#include <string>

class FieldBox
	:public Image
{
public:
	FieldBox();
	~FieldBox();

	void initialize();
	void setField(unsigned char* field, int bitCount, int width, int height);
	bool setGraphic(int handle, int chipWidth, int chipHeight);

	bool setFieldData(int handle, FieldReader& field, int layerIdx);

	// Override
	void setSize(int width, int height) { }
	// Override
	void draw();

private:
	unsigned char at(int x, int y) const;
	int get(int x, int y) const;

private:
	unsigned char* mp_field;
	int m_bitCount;
	int m_chipWidth;
	int m_chipHeight;
	int m_srcWidth;
	int m_srcHeight;
	int m_handle;
};

#endif // !__INCLUDED_FIELDBOX_H__
