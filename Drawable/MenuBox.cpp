/*! @file MenuBox.cpp
	@brief メニューウィンドウ 
	@author Shohei Masuda
*/

#include "MenuBox.h"

/*! @brief コンストラクタ */
MenuBox::MenuBox()
{
	this->initialize();
}

/** @brief デストラクタ */
MenuBox::~MenuBox() {}

void MenuBox::initialize()
{
	// 値の初期化
	m_chipWidth = m_chipHeight = 0;
	mp_handles[0] = -1;
	m_r = m_g = m_b = 0;
}

/*! @brief ポップアップウィンドウ枠に使用する画像ハンドルを指定
	@details 画像ファイルは，画像チップを指定したフォーマットで並べて置かなければならない
	@param[in] divHandles RPGツクール形式の枠画像ファイルのハンドル．サイズは16 
	@return 画像読み込み成功時にtrue
*/
void MenuBox::setImage(int divHandles[], int chipWidth, int chipHeight)
{
	// チップサイズ設定
	m_chipWidth = chipWidth;
	m_chipHeight = chipHeight;
	// ハンドルの整理
	mp_handles[0] = divHandles[0];			// 左上
	mp_handles[1] = divHandles[3];			// 右上
	mp_handles[2] = divHandles[12];			// 左下
	mp_handles[3] = divHandles[15];			// 右下
	mp_handles[4] = divHandles[1];			// 上
	mp_handles[5] = divHandles[13];			// 下
	mp_handles[6] = divHandles[4];			// 左
	mp_handles[7] = divHandles[7];			// 右
}

/*! @brief ポップアップウィンドウ枠に使用すつ画像ファイルを読み込む 
	@details 画像ファイルは，画像チップを指定したフォーマットで並べて置かなければならない 
	@return 画像読み込み成功時にtrue 
*/
bool MenuBox::loadImage(const char* filename, int chipWidth, int chipHeight)
{
	if (mp_handles[0] == -1) {
		// チップサイズ設定
		m_chipWidth = chipWidth;
		m_chipHeight = chipHeight;
		// 画像の読み込み（RGBツクールフォーマットの場合）
		int handles[16];
		if (LoadDivGraph(filename, 16, 4, 4, m_chipWidth, m_chipHeight, handles) == -1) {
			return false;
		}
		// 画像の指定
		this->setImage(handles, chipWidth, chipHeight);
	}
			
	return true;
}


/*! @brief ウィンドウの描画 */
void MenuBox::draw()
{
	const int mapWidth = m_width / m_chipWidth;
	const int mapHeight = m_height / m_chipHeight;	
	int w = m_x + m_width - m_chipWidth;
	int h = m_y + m_height - m_chipHeight;
	int chw = m_chipWidth / 4;
	int chh = m_chipHeight / 4;

	if (m_alpha > 0.0 && mp_handles[0] != -1) {
		if (m_alpha < 255.0) { SetDrawBlendMode(DX_BLENDMODE_ALPHA, (int)m_alpha / 2); }

		// 背景の描画
		DrawBox(m_x + chw, m_y + chh, m_x + m_width - chw, m_y + m_height - chh, GetColor(0, 0, 0), true);

		if (m_alpha < 255.0) { SetDrawBlendMode(DX_BLENDMODE_ALPHA, (int)m_alpha); }

		// 隅の描画
		DrawGraph(m_x, m_y, mp_handles[0], true);				// 左上
		DrawGraph(w, m_y, mp_handles[1], true);					// 右上
		DrawGraph(m_x, h, mp_handles[2], true);					// 左下
		DrawGraph(w, h, mp_handles[3], true);					// 右下
		// 枠を描画する
		for (int i = 1; i < mapWidth - 1; i++)
		{
			int x = m_x + (i * m_chipWidth);
			DrawGraph(x, m_y, mp_handles[4], true);				// 上
			DrawGraph(x, h, mp_handles[5], true);				// 下
		}
		for (int i = 1; i < mapHeight - 1; i++)
		{
			int y = m_y + (i * m_chipHeight);
			DrawGraph(m_x, y, mp_handles[6], true);				// 左
			DrawGraph(w, y, mp_handles[7], true);				// 右
		}

		if (m_alpha < 255.0) { SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0); }
	}

}