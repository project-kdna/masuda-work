#ifndef __INCLUDED_DIAGRAMIMAGE_H__
#define __INCLUDED_DIAGRAMIMAGE_H__

#include "Utility.h"
#include "Image.h"
#include <DxLib.h>

/*! @brief 矩形クラス */
class RectangleImage
	: public Image
{
public:
	RectangleImage();
	RectangleImage(int x, int y, int width, int height, int color, bool isFill = true);
	~RectangleImage();

	void draw();

private:
	int m_color;				//!< 矩形の色
	bool m_isFill;				//!< 矩形を塗りつぶすかどうか
};

#endif // !__INCLUDED_DIAGRAMIMAGE_H__
