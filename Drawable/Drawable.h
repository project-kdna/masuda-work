
#ifndef __INCLUDED_DRAWABLE_H__
#define __INCLUDED_DRAWABLE_H__

class Drawable
{
public:
	Drawable() {}
	virtual ~Drawable() {}

	virtual void draw() = 0;

	void setPosition(int x, int y);
	void setAlpha(double alpha);
	int getX() const;
	int getY() const;
	int getWidth() const;
	int getHeight() const;
	double getAlpha() const;

protected:
	void setSize(int width, int height);

private:
	int m_x;
	int m_y;
	int m_width;
	int m_height;
	double m_alpha;
};


inline void Drawable::setPosition(int x, int y)
{
	m_x = x; m_y = y;
}

inline void Drawable::setSize(int width, int height)
{
	m_width = width; m_height = height;
}

inline void Drawable::setAlpha(double alpha)
{
	if (alpha < 0.0) { alpha = 0.0; }
	if (alpha > 1.0) { alpha = 1.0; }
	m_alpha = alpha;
}

inline int Drawable::getX() const { return m_x; }
inline int Drawable::getY() const { return m_y; }
inline int Drawable::getWidth() const { return m_width; }
inline int Drawable::getHeight() const { return m_height; }
inline double Drawable::getAlpha() const { return m_alpha; }

#endif // !__INCLUDED_DRAWABLE_H__

