#ifndef __INCLUDED_GRAPHICIMAGE_H__
#define __INCLUDED_GRAPHICIMAGE_H__

#include "Drawable.h"

/*! @brief 画像クラス */
class GraphicImage
	:public Drawable
{
public:
	GraphicImage();
	virtual ~GraphicImage();

	bool setGraphicHandle(int handle);
	bool setGraphicHandle(int handle, int x, int y, double alpha);

	bool isEmpty() const;

	// override
	void draw();

private:
	int m_handle;				//!< 画像ハンドル
};

inline bool GraphicImage::isEmpty() const { return (m_handle == -1); }

#endif // !__INCLUDED_GRAPHICIMAGE_H__
