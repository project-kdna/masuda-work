#include "FieldBox.h"

FieldBox::FieldBox()
{
	mp_field = nullptr;
	m_handle = -1;
	this->initialize();
}


FieldBox::~FieldBox()
{
	delete[] mp_field;
}


void FieldBox::initialize() {}


void FieldBox::setField(unsigned char* field, int bitCount, int width, int height)
{
	// チェック
	if (mp_field != nullptr) { return; }

	// レイヤーをコピーする
	int size = width * height * (bitCount / 8);
	mp_field = new unsigned char[size];
	memcpy(mp_field, field, size);
	// 登録
	m_width = width;
	m_height = height;
	m_bitCount = bitCount;
}

bool FieldBox::setGraphic(int handle, int chipWidth, int chipHeight)
{
	// グラフィックのサイズを得る
	int gw, gh;
	GetGraphSize(handle, &gw, &gh);
	// 登録
	m_handle = handle;
	m_chipWidth = chipWidth;
	m_chipHeight = chipHeight;
	m_srcWidth = gw / chipWidth;
	m_srcHeight = gh / chipHeight;
	return true;
}

bool FieldBox::setFieldData(int handle, FieldReader& field, int layerIdx)
{
	this->setField(field.getLayer(layerIdx),
				   field.getBitCount(),
				   field.getWidth(),
				   field.getHeight()
				   );

	this->setGraphic(handle,
					 field.getChipWidth(),
					 field.getChipHeight()
					 );
	return true;
}


void FieldBox::draw()
{
	// チェック
	if (mp_field == nullptr || m_handle == -1) { return; }
	// マップの描画
	for (int y = 0; y < m_height; y++)
	{
		for (int x = 0; x < m_width; x++)
		{
			int srcIdx = get(x, y);
			int srcX = (srcIdx % m_srcWidth) * m_chipWidth;
			int srcY = (srcIdx / m_srcWidth) * m_chipHeight;

			// 描画
			DrawRectGraph(
				(x * m_chipWidth) + m_x, (y * m_chipHeight) + m_y,
				srcX, srcY,
				m_chipWidth, m_chipHeight,
				m_handle,
				true, false);
		}
	}
}


int FieldBox::get(int x, int y) const
{
	// チェック
	if (x >= m_width || y >= m_height) { return -1; }

	// ポインタ移動
	unsigned char* p = mp_field + (y * m_width + x) * (m_bitCount / 8);
	// 8bitレイヤー
	if (m_bitCount == 8) { return static_cast<int>(*p); }
	// 16bitレイヤー
	if (m_bitCount == 16) {	return static_cast<int>(*(p + 1) << 8 | (*p));
	}

	return -1;
}