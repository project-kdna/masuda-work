#ifndef __INCLUDED_TEXTBOX_H__
#define __INCLUDED_TEXTBOX_H__

#include "Drawable.h"
#include <DxLib.h>
#include <sstream>
#include <clocale>

class TextBox
	: public Drawable
{
public:
	TextBox();
	TextBox(char* fontname, int size, int thick);
	~TextBox();

	void initialize();
	void create(int x, int y, int width, int height);
	void clear();
	void draw();
	static void substr(char* str, char* sub, int start, int len);
	static bool isJapaneseSJIS(char c) { return (c >= 0x81 && c <= 0x9F) || (c >= 0xE0 && c <= 0xFC); }

	void setFont(char* fontname, int size, int thick);
	void setDrawString(char* str);
	void setDelay(double delay) { m_delay = delay; }

private:
	std::stringstream m_sBuffer;
	char m_buffer[327680];		//!< 保持文字数（約20480文字）
	int m_r, m_g, m_b;
	int m_fontHandle;
	double m_delay;
};

#endif // !__INCLUDED_TEXTBOX_H__