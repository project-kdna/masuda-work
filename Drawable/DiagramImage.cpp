#include "DiagramImage.h"

/*! @brief コンストラクタ */
RectangleImage::RectangleImage()
{
	m_color = 0x000000;
	m_isFill = true;
}

/*! @brief コンストラクタ */

RectangleImage::RectangleImage(int x, int y, int width, int height, int color, bool isFill)
{
	setPosition(x, y);
	setSize(width, height);
	m_color = color;
	m_isFill = isFill;
}

/*! @brief デストラクタ */
RectangleImage::~RectangleImage() {}

/*! @brief 矩形を描画 */
void RectangleImage::draw()
{
	int r, g, b;
	util::cvtColorToRGB(m_color, r, g, b);

	// 矩形を描画する
	if (m_alpha > 0.0) {
		if (m_alpha < 1.0) { SetDrawBlendMode(DX_BLENDMODE_ALPHA, (int)(m_alpha * 255.0)); }
		DrawBox(m_x, m_y, m_x + m_width, m_y + m_height, GetColor(r, g, b), m_isFill);
		if (m_alpha < 1.0) { SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0); }
	}
}