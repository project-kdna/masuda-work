#include "TextImage.h"
#include "Utility.h"
#include <DxLib.h>
#include <cstdarg>

/*! @brief コンストラクタ */
TextImage::TextImage()
{
	this->setPosition(0, 0);
	this->setSize(0, 0);
	this->setFontHandle(-1);
	this->setTextColor(0xFFFFFF);
	this->setLineSpace(0);

	m_strings.reserve(10);
}

TextImage::TextImage(int x, int y, const char* str)
{
	setText(x, y, str);
	this->setFontHandle(-1);
	this->setTextColor(0xFFFFFF);
	this->setLineSpace(0);
}

/*! @brief デストラクタ */
TextImage::~TextImage() {}

/*!
 *	@brief 表示するフォントを設定する 
 *	@param[in] fontHandle フォントハンドル 
*/
void TextImage::setFontHandle(int handle)
{
	// フォントの登録
	m_fontHandle = handle;
	// デフォルトの取得
	if (m_fontHandle == -1) { m_fontHandle = GetDefaultFontHandle(); }
	// フォントサイズを取得
	m_textSize = GetFontSizeToHandle(m_fontHandle);

	// フォントサイズの取得
	this->setTextSize(GetFontSizeToHandle(m_fontHandle));
}

/*! @brief 描画する文字列を設定する
*	@param[in] str 文字列
*/
void TextImage::setText(const char* str)
{
	// 文字列をエスケープに合わせて分割する．
	m_strings = this->split(str, "\n");
	// 文字列の最大横幅を計算する
	int maxWidth = 0;
	int lines = -1;
	std::vector<std::string>::iterator it;
	for (it = m_strings.begin(); it != m_strings.end(); ++it)
	{
		int w = GetDrawStringWidthToHandle(it->c_str(), it->length(), m_fontHandle);
		if (maxWidth < w) { maxWidth = w; }
		lines++;
	}

	// 文字列の幅を取得する
	int width = maxWidth;
	// 文字列の全体高さを取得する．
	int height = m_textSize + lines * (m_lineSpace + m_textSize);
	// 設定する
	this->setSize(width, height);
}

/*! @brief 描画する文字列を設定する 
	@param[in] str 文字列
*/
void TextImage::setText(int x, int y, const char* str)
{
	this->setPosition(x, y);
	this->setText(str);
}

/*! @brief 描画する文字列を設定する 
	@param[in] fmt フォーマット文字列
*/
void TextImage::setFormatText(const char* fmt, ...)
{
	char buf[1024];
	va_list ap;
	va_start(ap, fmt);
	vsprintf_s(buf, 1024, fmt, ap);
	va_end(ap);

	this->setText(buf);
}

void TextImage::draw()
{
	double alpha = this->getAlpha();
	int alphaInt = static_cast<int>(alpha * 255.0);
	int x = this->getX();
	int y = this->getY();

	if (alpha > 0.0) {
		if (alpha < 1.0) { SetDrawBlendMode(DX_BLENDMODE_ALPHA, alphaInt); }

		int r, g, b;
		util::cvtColorToRGB(m_color, r, g, b);

		// 上手く改行して表示する
		int lines = 0;
		std::vector<std::string>::iterator it;
		for (it = m_strings.begin(); it != m_strings.end(); ++it)
		{
			DrawStringToHandle(x, y + lines * (m_textSize + m_lineSpace), it->c_str(), GetColor(r, g, b), m_fontHandle);
			lines++;
		}
		
		if (alpha < 1.0) { SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0); }
	}
}